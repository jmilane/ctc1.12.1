package com.oneviewcommerce.util;

import java.math.BigDecimal;

public class PriceUtil {
	public static long getValueFromFloat(float initValue) {
		BigDecimal bd = new BigDecimal(initValue);
		bd = bd.multiply(new BigDecimal(100));
		return bd.longValue();
	}
	
	public static long recalculateValue(long initValue, float percentage) {
		BigDecimal bd = new BigDecimal(initValue);
		bd = bd.multiply(new BigDecimal(1.0 - percentage));
		return bd.longValue();
	}
}
