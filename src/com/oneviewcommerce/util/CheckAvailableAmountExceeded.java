package com.oneviewcommerce.util;

import java.util.Iterator;
import java.util.List;

import com.enactor.cashManagement.listElements.ITenderListElement;
import com.enactor.core.application.process.ApplicationProcessDataType;
import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessDataType;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.annotations.Output;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;

@Inputs( { @Input(name = "enactor.cashManagement.TendersList", type = java.util.List.class, required = true) })
@Outputs( { @Output(name = "enactor.pos.AvailableAmountExceeded", type = java.lang.Boolean.class) })
@Outcomes( { "Fail", "Success" })
public class CheckAvailableAmountExceeded implements IUIAction {

	/*******************************************************
	 * Constants
	 ******************************************************/

	/** Default Serial Version */
	private static final long serialVersionUID = 1L;
	/*******************************************************
	 * Properties
	 ******************************************************/

	/** Logger */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(CheckAvailableAmountExceeded.class.getName());

	/*******************************************************
	 * IUIAction Interface
	 ******************************************************/

	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		// Tenders List
		List<ITenderListElement> tendersList = null;
		boolean amountExceeded = false;
		if (inputData.containsNotNullData("enactor.cashManagement.TendersList")) {
			tendersList = (List) inputData.getData("enactor.cashManagement.TendersList");
			Iterator<com.enactor.cashManagement.listElements.ITenderListElement> items = tendersList.iterator();
			while (items.hasNext()) {
				ITenderListElement tenderElement = items.next();
				if (tenderElement.getAvailableAmountExceeded()) {
					amountExceeded = true;
					break;
				}
			}

		} else {
			throw new UIProcessException(UIProcessException.MISSING_DATA,
					"The variable 'enactor.cashManagement.TendersList' is required as an input");

		}
		IApplicationProcessDataType availableAmountExceeded = new ApplicationProcessDataType("enactor.pos.AvailableAmountExceeded",
				java.lang.Boolean.class.getName());

		outputData.setData(availableAmountExceeded, amountExceeded);

		return CoreUIOutcomes.SUCCESS_OUTCOME;

	}

}
