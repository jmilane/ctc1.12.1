package com.oneviewcommerce.util;

import java.util.Iterator;
import java.util.List;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.entities.PropertyValidationException;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.annotations.Output;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.location.ILocation;
import com.enactor.mfc.tender.ICashTender;
import com.enactor.mfc.tender.ITender;
import com.oneviewcommerce.ctc.extendedentities.IExtendedLocation;
import com.oneviewcommerce.ctc.extendedentities.IExtendedStore;

@Inputs( { @Input(name = "enactor.mfc.Location", type = com.enactor.mfc.location.ILocation.class, required = true),
		@Input(name = "enactor.coreUI.List", type = java.util.List.class, required = true) })
@Outputs( { @Output(name = "enactor.coreUI.List", type = java.util.List.class) })
@Outcomes( { "Success" })
public class SetCashDrawerLocationLimit implements IUIAction {

	/*******************************************************
	 * Constants
	 ******************************************************/

	/** Default Serial Version */
	private static final long serialVersionUID = 1L;
	/*******************************************************
	 * Properties
	 ******************************************************/

	/** Logger */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(SetCashDrawerLocationLimit.class.getName());

	/*******************************************************
	 * IUIAction Interface
	 ******************************************************/

	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		// Region Key
		ILocation location = null;
		String regionId = "";
		long drawerLimit = 0;
		if (inputData.containsNotNullData("enactor.mfc.Location")) {
			location = (ILocation) inputData.getData("enactor.mfc.Location");

			if (location != null && location.getRegionGroupKey() != null && location.getRegionGroupKey().getGroupId() != null
					&& location.getRegionGroupKey().getGroupId().equals("FGLF")) {
				if (location instanceof com.enactor.mfc.location.Store)
					drawerLimit = ((IExtendedStore) location).getDrawerLimit();
				else if (location instanceof com.enactor.mfc.location.Location)
					drawerLimit = ((IExtendedLocation) location).getDrawerLimit();
			}
			// List
			List list = null;
			if (inputData.containsNotNullData("enactor.coreUI.List")) {
				list = (List) inputData.getData("enactor.coreUI.List");
				Iterator<ITender> listItr = list.iterator();
				while (listItr.hasNext()) {
					ITender tender = (ITender) listItr.next();
					if (tender instanceof ICashTender && drawerLimit > 0) {
						try {
							((ICashTender) tender).setDrawerLimit(drawerLimit);
						} catch (PropertyValidationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

			} else {
				throw new UIProcessException(UIProcessException.MISSING_DATA, "The variable 'enactor.coreUI.List' is required as an input");
			}

		} else {
			throw new UIProcessException(UIProcessException.MISSING_DATA, "The variable 'enactor.mfc.RegionKey' is required as an input");
		}

		return CoreUIOutcomes.SUCCESS_OUTCOME;

	}

}
