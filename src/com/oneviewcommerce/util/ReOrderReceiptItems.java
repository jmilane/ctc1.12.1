package com.oneviewcommerce.util;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.BasketException;
import com.enactor.mfc.basket.items.BasketItem;
import com.enactor.mfc.basket.items.IBasketItem;
import com.enactor.mfc.basket.items.IDiscountItem;
import com.enactor.mfc.basket.items.IMerchandiseItem;
import com.enactor.mfc.basket.items.IPriceOverrideItem;
import com.enactor.mfc.basket.items.VoidItem;
import com.enactor.mfc.retail.transaction.IRetailTransactionHandler;
import com.enactor.mfc.retail.transaction.NormalRetailSaleTransaction;

import edu.emory.mathcs.backport.java.util.Collections;

@Inputs( { @Input(name = "enactor.mfc.TransactionHandler", type = com.enactor.mfc.retail.transaction.IRetailTransactionHandler.class) })
@Outputs( {})
@Outcomes( { "Success", "Fail" })
public class ReOrderReceiptItems implements IUIAction {

	/*******************************************************
	 * Constants
	 ******************************************************/

	/** Default Serial Version */
	private static final long serialVersionUID = 1L;
	/*******************************************************
	 * Properties
	 ******************************************************/

	/** Logger */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(ReOrderReceiptItems.class.getName());

	/*******************************************************
	 * IUIAction Interface
	 ******************************************************/

	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		// Transaction Handler
		IRetailTransactionHandler transactionHandler = null;
		NormalRetailSaleTransaction retailTransaction = null;
		BasketItem itemAdded = null;
		boolean isDiscountItem = false;
		boolean isVoid = false;
		if (inputData.containsNotNullData("enactor.mfc.TransactionHandler")) {
			transactionHandler = (IRetailTransactionHandler) inputData.getData("enactor.mfc.TransactionHandler");

			if (inputData.containsNotNullData("enactor.mfc.BasketItem"))
				itemAdded = (BasketItem) inputData.getData("enactor.mfc.BasketItem");
			if (itemAdded instanceof IDiscountItem)
				isDiscountItem = true;
			else if (itemAdded instanceof IPriceOverrideItem)
				isDiscountItem = false;
			else if (itemAdded instanceof VoidItem)
				isVoid = true;

			try {
				retailTransaction = (NormalRetailSaleTransaction) transactionHandler.getTransaction();
				if (retailTransaction == null)
					return CoreUIOutcomes.SUCCESS_OUTCOME;

				IMerchandiseItem item;
				IDiscountItem discountItem = null;
				IPriceOverrideItem priceOverrideItem = null;

				int basketCount = retailTransaction.getBasket().getItemCount();
				Map productLineNumber = new HashMap();
				int discountLineNumber = 0;
				int lineNumber = 0;
				boolean itemEdited = false;

				//Item Void
				int voidedLine = 0;

				if (isVoid) {
					voidedLine = ((VoidItem) itemAdded).getVoidedLine();
					if (basketCount > 0) {
						for (int i = 1; i <= basketCount; i++) {
							try {
								if (!itemEdited && retailTransaction.getBasket().getItem(i) instanceof VoidItem) {
									if (((VoidItem) retailTransaction.getBasket().getItem(i)).equals((VoidItem) itemAdded)) {
										retailTransaction.getBasket().getItem(i).setLineNumber(voidedLine + 1);
										itemEdited = true;
										i = 1;

									}
								}
								if (itemEdited && retailTransaction.getBasket().getItem(i).getLineNumber() >= voidedLine + 1) {
									if (retailTransaction.getBasket().getItem(i) instanceof VoidItem
											&& retailTransaction.getBasket().getItem(i).getLineNumber() == voidedLine + 1) {
										continue;
									}

									lineNumber = retailTransaction.getBasket().getItem(i).getLineNumber();
									retailTransaction.getBasket().getItem(i).setLineNumber(lineNumber + 1);
								}

							} catch (BasketException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						if (itemEdited) {
							List itemsList = retailTransaction.getBasket().getItemsList();

							if (itemsList != null) {
								Collections.sort(itemsList, new BasketSort());
							}
						}
					}

					return CoreUIOutcomes.SUCCESS_OUTCOME;
				}

				// loop through the basket to find which item we have to tax exempt and find the tax amount which we have to deduct ..
				String prevProductId = "";
				if (basketCount > 0) {
					for (int i = 1; i <= basketCount; i++) {
						try {
							if ((retailTransaction.getBasket().getItem(i) instanceof IMerchandiseItem)) {

								item = (IMerchandiseItem) retailTransaction.getBasket().getItem(i);
								if (!"".equals(prevProductId))
									productLineNumber.put(prevProductId, item.getLineNumber());
								prevProductId = item.getProductID() + "_" + item.getLineNumber();
							}
						} catch (BasketException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				if (productLineNumber != null && productLineNumber.size() > 0) {
					if (basketCount > 0) {
						for (int i = 1; i <= basketCount; i++) {
							try {
								if (isDiscountItem && !itemEdited && (retailTransaction.getBasket().getItem(i) instanceof IDiscountItem)) {

									discountItem = (IDiscountItem) retailTransaction.getBasket().getItem(i);
									if (!discountItem.equals((IDiscountItem) itemAdded))
										continue;
									if (discountItem.getModifiedItem() instanceof IMerchandiseItem)
										if (productLineNumber.containsKey(((IMerchandiseItem) discountItem.getModifiedItem())
												.getProductID()
												+ "_" + discountItem.getModifiedItem().getLineNumber())) {
											discountLineNumber = (Integer) productLineNumber.get(((IMerchandiseItem) discountItem
													.getModifiedItem()).getProductID()
													+ "_" + discountItem.getModifiedItem().getLineNumber());
											retailTransaction.getBasket().getItem(i).setLineNumber(discountLineNumber);
											itemEdited = true;
											i = 0;
											continue;
										}
								}
								if (!isDiscountItem && !itemEdited
										&& (retailTransaction.getBasket().getItem(i) instanceof IPriceOverrideItem)) {

									priceOverrideItem = (IPriceOverrideItem) retailTransaction.getBasket().getItem(i);
									if (!priceOverrideItem.equals((IPriceOverrideItem) itemAdded))
										continue;
									if (priceOverrideItem.getModifiedItem() instanceof IMerchandiseItem)
										if (productLineNumber.containsKey(((IMerchandiseItem) priceOverrideItem.getModifiedItem())
												.getProductID()
												+ "_" + priceOverrideItem.getModifiedItem().getLineNumber())) {
											discountLineNumber = (Integer) productLineNumber.get(((IMerchandiseItem) priceOverrideItem
													.getModifiedItem()).getProductID()
													+ "_" + priceOverrideItem.getModifiedItem().getLineNumber());
											retailTransaction.getBasket().getItem(i).setLineNumber(discountLineNumber);
											itemEdited = true;
											i = 0;
											continue;
										}
								}

								if (itemEdited && retailTransaction.getBasket().getItem(i).getLineNumber() >= discountLineNumber) {

									if (isDiscountItem && retailTransaction.getBasket().getItem(i) instanceof IDiscountItem
											&& discountItem != null
											&& discountItem.equals((IDiscountItem) retailTransaction.getBasket().getItem(i))) {

										/*BasketItem newPriceBasketItem = new BasketItem();
										newPriceBasketItem.setLineNumber(discountLineNumber + 1);
										newPriceBasketItem.setNetValue(discountItem.getModifiedItem().getNetValue());
										newPriceBasketItem.setDescription("NEW_PRICE");
										newPriceBasketItem.addAdditionalDatum(((IMerchandiseItem) discountItem.getModifiedItem())
												.getProductID(), ((IMerchandiseItem) discountItem.getModifiedItem()).getProductID());

										retailTransaction.getBasket().addItem(newPriceBasketItem);*/
										continue;
									} else if (!isDiscountItem && retailTransaction.getBasket().getItem(i) instanceof IPriceOverrideItem
											&& priceOverrideItem != null
											&& priceOverrideItem.equals((IPriceOverrideItem) retailTransaction.getBasket().getItem(i))) {
										continue;
									}
									lineNumber = retailTransaction.getBasket().getItem(i).getLineNumber();
									retailTransaction.getBasket().getItem(i).setLineNumber(lineNumber + 1);

								}
							} catch (BasketException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						if (itemEdited) {
							//Sort the basket based on line number
							List itemsList = retailTransaction.getBasket().getItemsList();

							if (itemsList != null) {
								Collections.sort(itemsList, new BasketSort());
							}

						}
					}
				}
			} catch (Exception e) {
				return CoreUIOutcomes.FAIL_OUTCOME;
			}
		}

		return CoreUIOutcomes.SUCCESS_OUTCOME;

	}

	public class BasketSort implements Comparator<IBasketItem> {

	//	@Override
		public int compare(IBasketItem item1, IBasketItem item2) {

			if (item1 != null && item1.getLineNumber() > 0 && item2 != null && item2.getLineNumber() > 0)
				return item1.getLineNumber() - item2.getLineNumber();
			return 0;
		}
	}
}
