package com.oneviewcommerce.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.annotations.Output;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;

@Inputs( { @Input(name = "applicationHome", type = java.lang.String.class, required = true) })
@Outputs( { @Output(name = "applicationVersion", type = java.lang.String.class) })
@Outcomes( { "Success" })
public class GetVersionAction implements IUIAction {
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(GetVersionAction.class.getName());

	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		// application Home
		String applicationHome = null;
		String applicationVersion = "";
		if (inputData.containsNotNullData("applicationHome")) {
			applicationHome = (String) inputData.getData("applicationHome");
		} else {
			throw new UIProcessException(UIProcessException.MISSING_DATA, "The variable 'applicationHome' is required as an input");
		}

		SAXBuilder builder = new SAXBuilder();
		File file = new File(applicationHome, "manifest.xml");
		try {
			Document document = (Document) builder.build(file);
			Element rootNode = document.getRootElement();
			Namespace core = Namespace.getNamespace("http://www.enactor.com/core");
			List list = rootNode.getChildren("version", core);
			int i = 0;
			while (applicationVersion.isEmpty() && i < list.size()) {
				Object o = list.get(i);
				if (o instanceof Element) {
					String application = ((Element) o).getAttributeValue("applicationId");
					if (application.equals("Enactor Pos")) {
						applicationVersion = ((Element) o).getText();
					}
				}
				++i;
			}

		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (JDOMException jdomex) {
			System.out.println(jdomex.getMessage());
		}
		outputData.putStringData("enactor.mfc.PosVersion", applicationVersion);
		return CoreUIOutcomes.SUCCESS_OUTCOME;
	}
}
