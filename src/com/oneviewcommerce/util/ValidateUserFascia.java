package com.oneviewcommerce.util;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.database.DataAccessContext;
import com.enactor.core.database.DatabaseException;
import com.enactor.core.database.IDataAccessSession;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.location.ILocationKey;
import com.enactor.mfc.location.Location;
import com.enactor.mfc.location.LocationDBServer;
import com.enactor.mfc.location.Store;
import com.enactor.mfc.user.User;
import com.enactor.mfc.user.UserDBServer;
import com.enactor.mfc.user.UserKey;

@Inputs( { @Input(name = "enactor.mfc.FasciaId", type = java.lang.String.class),
		@Input(name = "enactor.signOn.UserId", type = java.lang.String.class) })
@Outputs( {})
@Outcomes( { "Fail", "Success" })
public class ValidateUserFascia implements IUIAction {

	/*******************************************************
	 * Constants
	 ******************************************************/

	/** Default Serial Version */
	private static final long serialVersionUID = 1L;
	/*******************************************************
	 * Properties
	 ******************************************************/

	/** Logger */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(ValidateUserFascia.class.getName());

	private IDataAccessSession session = null;

	/*******************************************************
	 * IUIAction Interface
	 ******************************************************/

	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		// Fascia Id
		// User Id
		String userId = null;
		String fasciaId = null;
		String locationFascia = null;

		if (inputData.containsNotNullData("enactor.mfc.FasciaId")) {
			fasciaId = (String) inputData.getData("enactor.mfc.FasciaId");

			if (inputData.containsNotNullData("enactor.signOn.UserId")) {
				userId = (String) inputData.getData("enactor.signOn.UserId");
			}

			if (DataAccessContext.isSessionOpen()) {
				try {
					session = DataAccessContext.getCurrentSession();
				} catch (DatabaseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return CoreUIOutcomes.FAIL_OUTCOME;
				}
			} else {
				try {
					session = DataAccessContext.openSession();
				} catch (DatabaseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return CoreUIOutcomes.FAIL_OUTCOME;
				}
			}

			UserDBServer userDbServer = new UserDBServer();
			try {
				User user = (User) userDbServer.load(new UserKey(userId), null);
				ILocationKey locationKey = (ILocationKey) user.getLocationKey();
				if (locationKey != null) {
					LocationDBServer locationDbServer = new LocationDBServer();
					Location<?> location = (Location<?>) locationDbServer.load(locationKey, null);
					if (location != null) {
						locationFascia = ((Store) location).getFasciaKey().getGroupId();
						if (fasciaId != null && locationFascia != null && locationFascia.equals(fasciaId)) {
							return CoreUIOutcomes.SUCCESS_OUTCOME;
						} else
							return CoreUIOutcomes.FAIL_OUTCOME;

					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return CoreUIOutcomes.FAIL_OUTCOME;
			} finally {
				if (session != null)
					try {
						session.close();
					} catch (DatabaseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}

		}

		return CoreUIOutcomes.SUCCESS_OUTCOME;

	}
}
