package com.oneviewcommerce.actions.basket;

import java.util.ArrayList;
import java.util.List;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.objectCache.CacheManager;
import com.enactor.core.transaction.TransactionException;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.actions.UIActionFunctions;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessContext;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.IBasket;
import com.enactor.mfc.basket.IModifier;
import com.enactor.mfc.basket.items.DiscountModifier;
import com.enactor.mfc.basket.items.IMerchandiseItem;
import com.enactor.mfc.basket.items.IModifiableItem;
import com.enactor.mfc.basket.items.IValueModifier;
import com.enactor.mfc.basket.items.MerchandiseItem;
import com.enactor.mfc.basket.items.TransactionDiscountItem;
import com.enactor.mfc.retail.transaction.NormalRetailSaleTransactionHandler;
import com.enactor.pos.processes.PosActionFunctions;

public class RemoveTxnDiscountModifiersAction implements IUIAction {

	private static final long serialVersionUID = 1L;

	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		NormalRetailSaleTransactionHandler transactionHandler = (NormalRetailSaleTransactionHandler) PosActionFunctions
				.getTransactionHandler(inputData);

		IBasket basket = transactionHandler.getModel().getBasket();

		IMerchandiseItem merchandiseItem = (IMerchandiseItem) UIActionFunctions.getRequiredDataItem(inputData,
				"enactor.mfc.MerchandiseItem");

		List<IModifier> modifiers = merchandiseItem.getModifiers();
		for (int i = modifiers.size() - 1; i > -1; --i) {
			IModifier modifier = modifiers.get(i);
			if (modifier instanceof DiscountModifier) {
				if (((DiscountModifier) modifier).getModifierItem() instanceof TransactionDiscountItem) {
					TransactionDiscountItem txnDiscountItem = (TransactionDiscountItem) ((DiscountModifier) modifier).getModifierItem();
					try {
						basket.removeItem(txnDiscountItem);
						for (int l = basket.getItemCount(); l > 0; --l) {
							if (basket.getItem(l) instanceof MerchandiseItem) {
								MerchandiseItem anotherMerchItem = (MerchandiseItem) basket.getItem(l);
								List<IModifier> moreModifiers = anotherMerchItem.getModifiers();
								for (int m = moreModifiers.size() - 1; m >= 0; --m) {
									IModifier anotherModifier = moreModifiers.get(m);
									if (anotherModifier instanceof DiscountModifier) {
										if (((DiscountModifier) anotherModifier).getModifierItem() instanceof TransactionDiscountItem) {
											TransactionDiscountItem anotherTxnDiscountItem = (TransactionDiscountItem) ((DiscountModifier) anotherModifier)
													.getModifierItem();
											if (anotherTxnDiscountItem.equals(txnDiscountItem)) {
												anotherMerchItem.removeModifier(m);
											}
										}
									}
									TxnDiscountBasketItemProcessor txnDiscountBasketItemProcessor = new TxnDiscountBasketItemProcessor();
									txnDiscountBasketItemProcessor.updateNetValueWithoutTxnDiscounts(anotherMerchItem);
								}
							}
						}

						List<IModifiableItem> modifiedItems = txnDiscountItem.getModifiedItems();
						for (int p = modifiedItems.size() - 1; p >= 0; --p) {
							ArrayList<IValueModifier> itemMods = (ArrayList<IValueModifier>) txnDiscountItem.getModifiers(modifiedItems
									.get(p));
							for (int q = 0; q < itemMods.size(); ++q) {
								txnDiscountItem.removeModifier(itemMods.get(q), modifiedItems.get(p));
							}
							if (modifiedItems.get(p).equals(merchandiseItem)) {
								txnDiscountItem.removeModifiedItem(modifiedItems.get(p));
							}
						}

						String cacheId = null;
						if (UIProcessContext.getContext() != null) {
							cacheId = (new StringBuilder()).append(
									UIProcessContext.getContext().getProcessRunner().getView().getInstanceId()).append(":").append(
									"EntityCache").toString();
							CacheManager.putCacheEntity(cacheId, "txnDiscountItem", txnDiscountItem);
						}
					} catch (TransactionException ext) {
						throw new UIProcessException(ext);
					}
				}
			}
		}
		return CoreUIOutcomes.SUCCESS_OUTCOME;
	}
}
