package com.oneviewcommerce.actions.basket;

import java.util.ArrayList;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.objectCache.CacheManager;
import com.enactor.core.transaction.TransactionException;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessContext;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.items.TransactionDiscountItem;
import com.enactor.mfc.retail.transaction.NormalRetailSaleTransactionHandler;
import com.enactor.pos.processes.PosActionFunctions;

public class ReapplyTxnDiscountItemsAction implements IUIAction {

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(ReapplyTxnDiscountItemsAction.class.getName());

	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		// get the TransactionDiscountItems to reapply from the cache
		if (UIProcessContext.getContext() != null) {
			final String cacheId = (new StringBuilder()).append(UIProcessContext.getContext().getProcessRunner().getView().getInstanceId())
					.append(":").append("EntityCache").toString();
			ArrayList<TransactionDiscountItem> txnDiscountItemsToRestore = CacheManager
					.getCacheEntity(cacheId, "txnDiscountItemsToRestore");

			if (null != txnDiscountItemsToRestore && 0 < txnDiscountItemsToRestore.size()) {
				NormalRetailSaleTransactionHandler transactionHandler = (NormalRetailSaleTransactionHandler) PosActionFunctions
						.getTransactionHandler(inputData);
				CTCTransactionDiscountItemProcessor processor = new CTCTransactionDiscountItemProcessor();

				// reapply them
				for (int i = 0; i < txnDiscountItemsToRestore.size(); ++i) {
					TransactionDiscountItem txnDiscountItem = txnDiscountItemsToRestore.get(i);
					try {
						transactionHandler.processTransactionItem(txnDiscountItem, processor);
					} catch (TransactionException e) {
						logger.log(Logger.LOG_ERROR, "Error in com.oneviewcommerce.actions.basket.ReapplyTxnDiscountItemsAction: "
								+ e.getMessage());
						throw new UIProcessException(e);
					}
				}
			}
		}
		return CoreUIOutcomes.SUCCESS_OUTCOME;
	}
}
