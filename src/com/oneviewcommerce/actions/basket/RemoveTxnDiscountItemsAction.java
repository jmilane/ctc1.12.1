package com.oneviewcommerce.actions.basket;

import java.util.ArrayList;
import java.util.List;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.objectCache.CacheManager;
import com.enactor.core.transaction.TransactionException;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessContext;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.BasketException;
import com.enactor.mfc.basket.IBasket;
import com.enactor.mfc.basket.IModifier;
import com.enactor.mfc.basket.items.DiscountModifier;
import com.enactor.mfc.basket.items.IModifiableItem;
import com.enactor.mfc.basket.items.IValueModifier;
import com.enactor.mfc.basket.items.MerchandiseItem;
import com.enactor.mfc.basket.items.TransactionDiscountItem;
import com.enactor.mfc.retail.transaction.NormalRetailSaleTransactionHandler;
import com.enactor.pos.processes.PosActionFunctions;

public class RemoveTxnDiscountItemsAction implements IUIAction {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(RemoveTxnDiscountItemsAction.class.getName());

	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		NormalRetailSaleTransactionHandler transactionHandler = (NormalRetailSaleTransactionHandler) PosActionFunctions
				.getTransactionHandler(inputData);

		ArrayList<TransactionDiscountItem> txnDiscountItemsToRestore = new ArrayList<TransactionDiscountItem>();
		IBasket basket = transactionHandler.getModel().getBasket();
		for (int i = basket.getItemCount(); i > 0; --i) {
			try {
				if (basket.getItem(i) instanceof TransactionDiscountItem) {
					// found a TransactionDiscountItem
					TransactionDiscountItem txnDiscountItem = (TransactionDiscountItem) basket.getItem(i);
					try {
						// remove it from the basket
						basket.removeItem(txnDiscountItem);

						// find out which items in the basket have this
						// TransactionDiscountItem
						for (int l = basket.getItemCount(); l > 0; --l) {
							if (basket.getItem(l) instanceof MerchandiseItem) {
								MerchandiseItem merchItem = (MerchandiseItem) basket.getItem(l);
								List<IModifier> merchModifiers = merchItem.getModifiers();
								for (int m = merchModifiers.size() - 1; m >= 0; --m) {
									IModifier merchModifier = merchModifiers.get(m);
									if (merchModifier instanceof DiscountModifier) {
										DiscountModifier discountModifier = (DiscountModifier) merchModifier;
										if (discountModifier.getModifierItem() instanceof TransactionDiscountItem) {
											TransactionDiscountItem modifierItem = (TransactionDiscountItem) discountModifier
													.getModifierItem();
											if (modifierItem.equals(txnDiscountItem)) {
												// remove the
												// TransactionDiscountItem from
												// the MerchandiseItem
												merchItem.removeModifier(m);
											}
										}
									}
									TxnDiscountBasketItemProcessor txnDiscountBasketItemProcessor = new TxnDiscountBasketItemProcessor();
									// update the value of the MerchandiseItem
									// without the TransactionDiscountItem
									txnDiscountBasketItemProcessor.updateNetValueWithoutTxnDiscounts(merchItem);
								}
							}
						}

						// clean up the TransactionDiscountItem, remove all the
						// modifiers from all the modified Items
						List<IModifiableItem> modifiedItems = txnDiscountItem.getModifiedItems();
						for (int p = modifiedItems.size() - 1; p >= 0; --p) {
							ArrayList<IValueModifier> itemMods = (ArrayList<IValueModifier>) txnDiscountItem.getModifiers(modifiedItems
									.get(p));
							for (int q = 0; q < itemMods.size(); ++q) {
								txnDiscountItem.removeModifier(itemMods.get(q), modifiedItems.get(p));
							}
						}

						// add this to the list of TransactionDiscountItems that
						// we'll want to put back later (i.e. after the void is
						// applied)
						txnDiscountItemsToRestore.add(txnDiscountItem);

					} catch (TransactionException ext) {
						throw new UIProcessException(ext);
					}
				}
			} catch (BasketException ex) {
				logger.log(Logger.LOG_ERROR, "BasketException in RemoveTxnDiscountItemsAction.java " + ex.getMessage());
				throw new UIProcessException(ex);
			}
		}

		// store the TransactionDiscountItems that we want to restore in the
		// cache
		String cacheId = null;
		if (UIProcessContext.getContext() != null) {
			cacheId = (new StringBuilder()).append(UIProcessContext.getContext().getProcessRunner().getView().getInstanceId()).append(":")
					.append("EntityCache").toString();
			CacheManager.putCacheEntity(cacheId, "txnDiscountItemsToRestore", txnDiscountItemsToRestore);
		}

		return CoreUIOutcomes.SUCCESS_OUTCOME;
	}
}
