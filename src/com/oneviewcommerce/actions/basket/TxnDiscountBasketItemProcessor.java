package com.oneviewcommerce.actions.basket;

import com.enactor.core.transaction.TransactionException;
import com.enactor.mfc.basket.items.IModifiableItem;
import com.enactor.mfc.basket.processors.BasketItemProcessor;

public class TxnDiscountBasketItemProcessor extends BasketItemProcessor {
	public void updateNetValueWithoutTxnDiscounts(IModifiableItem modifiableItem) throws TransactionException {
		updateNetValue(modifiableItem);
	}

}
