package com.oneviewcommerce.actions.basket;

import java.util.List;

import com.enactor.core.entities.PropertyValidationException;
import com.enactor.core.transaction.TransactionException;
import com.enactor.mfc.basket.IModifier;
import com.enactor.mfc.basket.items.IModifiableItem;
import com.enactor.mfc.basket.items.ITransactionDiscountItem;
import com.enactor.mfc.basket.items.IValueModifier;
import com.enactor.mfc.basket.items.ModifierType;
import com.enactor.mfc.basket.processors.TransactionDiscountItemProcessor;

public class ItemVoidTxnDiscountProcessor extends TransactionDiscountItemProcessor {
	protected void updateModifiers(ITransactionDiscountItem modifierItem) throws TransactionException {
		try {
			long itemsValue = 0L;
			List<IModifiableItem> modifiedItems = modifierItem.getModifiedItems();
			itemsValue = getTotalItemValue(modifiedItems);
			long totalModifierValue = getTotalModifierValue(modifierItem, itemsValue);
			modifierItem.setValue(totalModifierValue);
			modifierItem.setNetValue(0L);
			modifierItem.setEffectiveNetValue(0L);
			long remainingValue = totalModifierValue;
			for (int i = 0; i < modifiedItems.size(); i++) {
				IModifiableItem modifiedItem = (IModifiableItem) modifiedItems.get(i);
				IModifier modifier = getItemModifier(modifierItem, modifiedItem);
				if (!(modifier instanceof IValueModifier))
					throw new TransactionException("Modifier type invalid, must be value modifier");
				IValueModifier valueModifier = (IValueModifier) modifier;
				long modifierValue;
				if (i == modifiedItems.size() - 1)
					modifierValue = remainingValue;
				else
					modifierValue = Math.round((totalModifierValue * getModifiedItemValue(modifiedItem)) / itemsValue);
				if (modifierItem.getType() == ModifierType.PRICE_OVERRIDE)
					modifierValue = modifiedItem.getNetValue() <= 0L ? -modifierValue : modifierValue;
				remainingValue -= modifierValue;
				try {
					valueModifier.setValue(modifierValue);
					valueModifier.setEffectiveValue(modifierValue);
					valueModifier.setOriginalItemValue(getModifiedItemValue(modifiedItem));
				} catch (PropertyValidationException e) {
					throw new TransactionException(e);
				}
				modifierItem.addModifier(valueModifier, modifiedItem);
			}

		} catch (PropertyValidationException e) {
			throw new TransactionException(e);
		}
	}

}
