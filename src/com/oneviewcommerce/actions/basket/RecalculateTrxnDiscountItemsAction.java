package com.oneviewcommerce.actions.basket;

import java.util.ArrayList;
import java.util.List;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.IBasket;
import com.enactor.mfc.basket.IModifier;
import com.enactor.mfc.basket.items.DiscountModifier;
import com.enactor.mfc.basket.items.IModifiableItem;
import com.enactor.mfc.basket.items.IValueModifier;
import com.enactor.mfc.basket.items.MerchandiseItem;
import com.enactor.mfc.basket.items.TransactionDiscountItem;
import com.enactor.mfc.retail.transaction.RetailSaleTransactionHandler;
import com.enactor.pos.processes.PosActionFunctions;

public class RecalculateTrxnDiscountItemsAction implements IUIAction {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(RecalculateTrxnDiscountItemsAction.class.getName());

	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		IApplicationProcessOutcome outcome = CoreUIOutcomes.FAIL_OUTCOME;

		RetailSaleTransactionHandler transactionHandler = null;

		IBasket basket = null;
		try {

			transactionHandler = (RetailSaleTransactionHandler) PosActionFunctions.getTransactionHandler(inputData);
			basket = transactionHandler.getModel().getBasket();

			// see if the last item added is a MerchandiseItem
			if (basket.getItem(basket.getItemCount()) instanceof MerchandiseItem) {
				MerchandiseItem merchItem = (MerchandiseItem) basket.getItem(basket.getItemCount());
				// go through the entire basket and see if you have any
				// transaction
				// discount items
				for (int i = basket.getItemCount(); i > 0; --i) {
					if (basket.getItem(i) instanceof TransactionDiscountItem) {
						TransactionDiscountItem txnDiscountItem = (TransactionDiscountItem) basket.getItem(i);

						// add the merchandise item to the items modified by the
						// transaction discount item
						txnDiscountItem.addModifiedItem(merchItem);
						try {

							// remove the transaction discount item from the
							// basket
							basket.removeItem(txnDiscountItem);

							// remove all percentage or value discounts
							// and modifiers from merchandise items
							for (int l = basket.getItemCount(); l > 0; --l) {
								if (basket.getItem(l) instanceof MerchandiseItem) {
									MerchandiseItem anotherMerchItem = (MerchandiseItem) basket.getItem(l);
									List<IModifier> modifiers = anotherMerchItem.getModifiers();
									for (int m = modifiers.size() - 1; m >= 0; --m) {
										IModifier modifier = modifiers.get(m);
										if (modifier instanceof DiscountModifier) {
											final String name = modifier.getType().name();
											if (name.equals("PERCENTAGE") || name.equals("VALUE")) {
												anotherMerchItem.removeModifier(modifier);
											}
										}

										// remove discounts from the merch item
										TxnDiscountBasketItemProcessor txnDiscountBasketItemProcessor = new TxnDiscountBasketItemProcessor();
										txnDiscountBasketItemProcessor.updateNetValueWithoutTxnDiscounts(anotherMerchItem);
									}
								}
							}

							// remove the existing modifiers from
							// txnDiscountItem
							List<IModifiableItem> modifiedItems = txnDiscountItem.getModifiedItems();
							for (int p = 0; p < modifiedItems.size(); ++p) {
								ArrayList<IValueModifier> itemMods = (ArrayList<IValueModifier>) txnDiscountItem.getModifiers(modifiedItems
										.get(p));
								for (int q = 0; q < itemMods.size(); ++q) {
									txnDiscountItem.removeModifier(itemMods.get(q), modifiedItems.get(p));
								}
							}
							// add the updated txnDiscountItem back
							transactionHandler.processTransactionItem(txnDiscountItem, new CTCTransactionDiscountItemProcessor());
							outcome = CoreUIOutcomes.SUCCESS_OUTCOME;
						} catch (Exception e) {
							throw new UIProcessException(e);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.log(Logger.LOG_ERROR, "Error in com.oneviewcommerce.actions.basket.RemoveTransactionDiscount.");
			throw new UIProcessException(e);
		}

		return outcome;
	}

}
