package com.oneviewcommerce.actions.discounts;

import com.enactor.core.application.process.ApplicationProcessData;
import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.ApplicationProcessOutcome;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.database.DataAccessContext;
import com.enactor.core.database.IDataAccessSession;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.actions.UIBuiltInAction;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.annotations.Output;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.items.IMerchandiseItem;
import com.enactor.mfc.basket.items.IPriceOverridableItem;
import com.enactor.mfc.basket.items.IPriceOverrideItem;
import com.enactor.mfc.location.ILocation;
import com.enactor.mfc.location.LocationDataTypes;
import com.enactor.mfc.product.IMerchandiseProduct;
import com.enactor.mfc.product.ProductDataTypes;
import com.enactor.mfc.product.actions.ProductLookupAction;
import com.oneviewcommerce.util.PriceUtil;

@Inputs( { @Input(name = "enactor.coreUI.UseViewCache", type = java.lang.Boolean.class, required = true),
		@Input(name = "enactor.mfc.PriceOverrideItem", type = com.enactor.mfc.basket.items.IPriceOverrideItem.class, required = true),
		@Input(name = "enactor.mfc.PriceOverridableItem", type = com.enactor.mfc.basket.items.IPriceOverridableItem.class) })
@Outputs( { @Output(name = "ctc.pos.CostPrice", type = java.lang.Long.class) })
@Outcomes( { "UnderCost", "Success" })
//
public class CheckCostPriceOverrideAction extends UIBuiltInAction {

	/*******************************************************
	 * Constants
	 ******************************************************/

	public final static IApplicationProcessOutcome INVALID_PRICE = new ApplicationProcessOutcome("enactor.pos.InvalidPrice");

	/** Default Serial Version */
	private static final long serialVersionUID = 1L;
	/*******************************************************
	 * Properties
	 ******************************************************/

	/** Logger */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(CheckCostPriceOverrideAction.class.getName());

	/*******************************************************
	 * IUIAction Interface
	 ******************************************************/

	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		ILocation location = (ILocation) this.getProcess().getView().getProcessData().get("enactor.mfc.Location");

		// Use View Cache
		Boolean useViewCache = null;
		if (inputData.containsNotNullData("enactor.coreUI.UseViewCache")) {
			useViewCache = (Boolean) inputData.getData("enactor.coreUI.UseViewCache");
		} else {
			throw new UIProcessException(UIProcessException.MISSING_DATA,
					"The variable 'enactor.coreUI.UseViewCache' is required as an input");
		}

		// Price Override Item
		IPriceOverrideItem priceOverrideItem = null;
		if (inputData.containsNotNullData("enactor.mfc.PriceOverrideItem")) {
			priceOverrideItem = (IPriceOverrideItem) inputData.getData("enactor.mfc.PriceOverrideItem");
		} else {
			throw new UIProcessException(UIProcessException.MISSING_DATA,
					"The variable 'enactor.mfc.PriceOverrideItem' is required as an input");
		}

		// Price Overridable Item
		IPriceOverridableItem priceOverridableItem = null;
		if (inputData.containsNotNullData("enactor.mfc.PriceOverridableItem")) {
			priceOverridableItem = (IPriceOverridableItem) inputData.getData("enactor.mfc.PriceOverridableItem");
		}

		String productId = ((IMerchandiseItem) priceOverridableItem).getProductID();

		IApplicationProcessData lookupOutput = new ApplicationProcessData();
		ProductLookupAction lookup = new ProductLookupAction();
		try {
			IDataAccessSession session = null;
			if (DataAccessContext.isSessionOpen()) {
				session = DataAccessContext.getCurrentSession();
			} else {
				session = DataAccessContext.openSession();
			}
			//session.beginTransaction();
			inputData.setData(ProductDataTypes.PRODUCT_CODE_DATA, productId);
			inputData.setData(LocationDataTypes.LOCATION_DATA, location);
			lookup.execute(inputData, lookupOutput);
			session.close();

			IMerchandiseProduct product = (IMerchandiseProduct) lookupOutput.get(ProductDataTypes.PRODUCT_DATA);

			long costValue = PriceUtil.getValueFromFloat(product.getStandardCostPrice());

			long itemValue = priceOverridableItem.getEffectiveNetValue();

			long newValue = priceOverrideItem.getNewPrice();

			/*if (itemValue < newValue) {
				outputData.put("ctc.pos.CostPrice", new Long(itemValue));
				return INVALID_PRICE;
			}*/

			if (newValue >= 0) {
				itemValue = newValue;
			}

			if (itemValue <= costValue) {
				outputData.put("ctc.pos.CostPrice", new Long(itemValue));
				return UNDER_COST;
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return CoreUIOutcomes.SUCCESS_OUTCOME;

	}

	private static final IApplicationProcessOutcome UNDER_COST = new ApplicationProcessOutcome("UnderCost");

}
