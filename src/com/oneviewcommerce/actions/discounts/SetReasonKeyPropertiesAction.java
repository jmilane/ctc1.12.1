package com.oneviewcommerce.actions.discounts;


import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.mfc.location.ILocation;
import com.enactor.mfc.reason.ReasonKey;
import com.enactor.core.entities.IEntityKey;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.annotations.Output;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.actions.DataSessionManagementType;
import com.enactor.coreUI.annotations.SessionManagement;

public class SetReasonKeyPropertiesAction implements IUIAction {

	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(SetReasonKeyPropertiesAction.class.getName());

	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
	throws UIProcessException, ApplicationProcessException {

		// Location
		ILocation location = null;
		String reasonId = null;
		if (inputData.containsNotNullData("enactor.mfc.Location")) {
			location = (ILocation)inputData.getData("enactor.mfc.Location");
		} else {
			throw new UIProcessException(UIProcessException.MISSING_DATA, "The variable 'enactor.mfc.Location' is required as an input");
		}

		if (inputData.containsNotNullData("enactor.pos.ReasonId")) {
			reasonId = (String)inputData.getData("enactor.pos.ReasonId");
		} else {
			throw new UIProcessException(UIProcessException.MISSING_DATA, "The variable 'enactor.pos.ReasonId' is required as an input");
		}

		
		// Key
		ReasonKey key = null;
		if (inputData.containsNotNullData("enactor.coreUI.Key")) {
			try{
			
			key = (ReasonKey)inputData.getData("enactor.coreUI.Key");
			key.setReasonId(reasonId);
			key.setGroupKey(location.getRegionGroupKey());
			outputData.put("enactor.coreUI.Key", key);
			
			}
			catch(Exception e)
			{
				System.out.println(" Error in SetReasonKeyPropertiesAction " + e);
			}
			}

		return CoreUIOutcomes.SUCCESS_OUTCOME;

	}

}