package com.oneviewcommerce.actions.discounts;

import com.enactor.core.application.process.ApplicationProcessData;
import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.ApplicationProcessOutcome;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.database.DataAccessContext;
import com.enactor.core.database.IDataAccessSession;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.actions.UIBuiltInAction;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.items.IDiscountItem;
import com.enactor.mfc.basket.items.IDiscountableItem;
import com.enactor.mfc.basket.items.IMerchandiseItem;
import com.enactor.mfc.location.ILocation;
import com.enactor.mfc.location.LocationDataTypes;
import com.enactor.mfc.product.IMerchandiseProduct;
import com.enactor.mfc.product.ProductDataTypes;
import com.enactor.mfc.product.actions.ProductLookupAction;
import com.oneviewcommerce.util.PriceUtil;

@Inputs( { @Input(name = "enactor.mfc.DiscountableItem", type = com.enactor.mfc.basket.items.IDiscountableItem.class) })
@Outputs( {})
@Outcomes( { "UnderCost", "Success" })
public class CheckCostPriceAction extends UIBuiltInAction {

	/*******************************************************
	 * Constants
	 ******************************************************/

	/** Default Serial Version */
	private static final long serialVersionUID = 1L;
	/*******************************************************
	 * Properties
	 ******************************************************/

	/** Logger */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(CheckCostPriceAction.class.getName());

	/*******************************************************
	 * IUIAction Interface
	 ******************************************************/

	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		ILocation location = (ILocation) this.getProcess().getView().getProcessData().get("enactor.mfc.Location");

		// Discountable Item
		IDiscountableItem discountableItem = null;
		if (inputData.containsNotNullData("enactor.mfc.DiscountableItem")) {
			discountableItem = (IDiscountableItem) inputData.getData("enactor.mfc.DiscountableItem");
		}

		// Discount Item
		IDiscountItem discountItem = null;
		if (inputData.containsNotNullData("enactor.mfc.DiscountItem")) {
			discountItem = (IDiscountItem) inputData.getData("enactor.mfc.DiscountItem");
		}

		String productId = ((IMerchandiseItem) discountableItem).getProductID();

		IApplicationProcessData lookupOutput = new ApplicationProcessData();
		ProductLookupAction lookup = new ProductLookupAction();
		try {
			IDataAccessSession session = null;
			if (DataAccessContext.isSessionOpen()) {
				session = DataAccessContext.getCurrentSession();
			} else {
				session = DataAccessContext.openSession();
			}
			//session.beginTransaction();
			inputData.setData(ProductDataTypes.PRODUCT_CODE_DATA, productId);
			inputData.setData(LocationDataTypes.LOCATION_DATA, location);
			lookup.execute(inputData, lookupOutput);
			session.close();

			IMerchandiseProduct product = (IMerchandiseProduct) lookupOutput.get(ProductDataTypes.PRODUCT_DATA);

			long costValue = PriceUtil.getValueFromFloat(product.getStandardCostPrice());

			long itemValue = discountableItem.getEffectiveNetValue();

			long discountValue = discountItem.getValue();

			float discountPercentage = discountItem.getPercentage();

			if (discountValue > 0) {
				itemValue = itemValue - discountValue;
			} else if (discountPercentage > 0) {
				itemValue = PriceUtil.recalculateValue(itemValue, discountPercentage);
			}

			if (itemValue <= costValue) {
				outputData.put("ctc.pos.CostPrice", new Long(itemValue));
				return UNDER_COST;
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return CoreUIOutcomes.SUCCESS_OUTCOME;

	}

	private static final IApplicationProcessOutcome UNDER_COST = new ApplicationProcessOutcome("UnderCost");

}
