package com.oneviewcommerce.actions.discounts;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.actions.UIActionFunctions;
import com.enactor.coreUI.processes.CoreUIDataTypes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import java.util.Iterator;

import com.enactor.mfc.basket.BasketDataTypes;
import com.enactor.mfc.basket.IBasket;
import com.enactor.mfc.basket.items.DiscountItem;
import com.enactor.mfc.basket.items.IBasketItem;
import com.enactor.mfc.basket.items.IDiscountItem;
import com.enactor.mfc.basket.items.IMerchandiseItem;
import com.enactor.mfc.basket.items.ISalesItem;
import com.enactor.mfc.basket.items.MerchandiseItem;

import java.lang.Object;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.annotations.Output;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.actions.DataSessionManagementType;
import com.enactor.coreUI.annotations.SessionManagement;

@Inputs({
	@Input(name = "enactor.mfc.BasketItemIterator", type = java.util.Iterator.class),
	@Input(name = "enactor.mfc.Basket", type = com.enactor.mfc.basket.IBasket.class, required = true),
	@Input(name = "enactor.pos.VisibilityTestData1", type = java.lang.Object.class),
	@Input(name = "enactor.pos.VisibilityTestData2", type = java.lang.Object.class),
	@Input(name = "enactor.coreUI.ListItemVisibilityTest", type = com.enactor.coreUI.actions.IUIAction.class, required = true)
})
@Outputs({
	@Output(name = "enactor.mfc.BasketItemIterator", type = java.util.Iterator.class),
	@Output(name = "enactor.mfc.BasketItem", type = com.enactor.mfc.basket.items.IBasketItem.class)
})
@Outcomes({
	"Completed", "Next"
})
public class TaxExemptItemLoop implements IUIAction {

	/*******************************************************
	 * Constants
	 ******************************************************/
	
	/** Default Serial Version */
	private static final long serialVersionUID = 1L;
	/*******************************************************
	 * Properties
	 ******************************************************/
	
	/** Logger */
	@SuppressWarnings("unused")private static Logger logger = Logger.getLogger(TaxExemptItemLoop.class.getName());

	/*******************************************************
	 * IUIAction Interface
	 ******************************************************/
	
	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
	throws UIProcessException, ApplicationProcessException {
	
	
		System.out.println("Inside TaxExemptItemLoop" );
    	
	
	
	// Visibility Test Data1
	Object visibilityTestData1 = null;
	if (inputData.containsNotNullData("enactor.pos.VisibilityTestData1")) {
	visibilityTestData1 = (Object)inputData.getData("enactor.pos.VisibilityTestData1");
	}
	
	// Visibility Test Data2
	Object visibilityTestData2 = null;
	if (inputData.containsNotNullData("enactor.pos.VisibilityTestData2")) {
	visibilityTestData2 = (Object)inputData.getData("enactor.pos.VisibilityTestData2");
	}
	
	
	Iterator basketItemIterator;
	
	
	
    IBasketItem basketItem;
  //  IUIAction listItemVisibilityTest = (IUIAction)UIActionFunctions.getRequiredDataItem(inputData, CoreUIDataTypes.LIST_ITEM_VISIBILITY_TEST);

    if (inputData.containsNotNullData(BasketDataTypes.BASKET_ITEM_ITERATOR_DATA)) 
    {
    	System.out.print("inside first if ");
      basketItemIterator = (Iterator)inputData.getData(BasketDataTypes.BASKET_ITEM_ITERATOR_DATA);
    }
    else 
    {
    	System.out.print("inside first else ");
        
      IBasket basket = (IBasket)UIActionFunctions.getRequiredDataItem(inputData, BasketDataTypes.BASKET_DATA);
      basketItemIterator = basket.getItemIterator(IDiscountItem.class);
      
      while (basketItemIterator.hasNext())
      {
      	basketItem = (IBasketItem)basketItemIterator.next();
      	System.out.println("Inside IDiscountItem basketItem" + basketItem);
      	if (basketItem instanceof IDiscountItem)
      	{
      		System.out.println("Inside IDiscountItem" );
      		//basketItemIterator.remove();
      		//basketItemIterator.remove();
      	}
      }
      basketItemIterator = basket.getItemIterator(ISalesItem.class);
      outputData.setData(BasketDataTypes.BASKET_ITEM_ITERATOR_DATA, basketItemIterator); 
    }
    
  //  basketItemIterator.
    
    while (basketItemIterator.hasNext())
    {
    	basketItem = (IBasketItem)basketItemIterator.next();
    	if (basketItem instanceof IMerchandiseItem)
    	{
    	
    	//	basketItem.get
    		System.out.println("Inside IMerchandiseItem" + ((IMerchandiseItem) basketItem).getProductID());
    		outputData.setData(BasketDataTypes.BASKET_ITEM_DATA, basketItem);
    		outputData.setData(BasketDataTypes.BASKET_ITEM_ITERATOR_DATA, basketItemIterator); 
    	    return CoreUIOutcomes.NEXT_OUTCOME;
    	}
    }
    return CoreUIOutcomes.COMPLETED_OUTCOME;
	
	
	}

}
