package com.oneviewcommerce.actions.discounts;

import com.enactor.core.application.process.ApplicationProcessDataType;
import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.ApplicationProcessOutcome;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessDataType;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.factories.EntityFactory;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.core.utilities.Logger;
import com.enactor.core.utilities.PropertyRegistry;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.mfc.productPrice.IProductPrice;
import com.enactor.mfc.productPrice.ProductPrice;
import com.enactor.mfc.reason.IItemDiscountReason;
import com.enactor.mfc.reason.IReasonKey;
import com.enactor.mfc.reason.ITaxOverrideReason;
import com.enactor.mfc.retail.transaction.IRetailTransactionHandler;
import com.enactor.mfc.retail.transaction.NormalRetailSaleTransaction;
import com.enactor.mfc.basket.BasketDataTypes;
import com.enactor.mfc.basket.items.IBasketItem;
import com.enactor.mfc.basket.items.IDiscountItem;
import com.enactor.mfc.basket.items.IDiscountableItem;
import com.enactor.mfc.basket.items.IMerchandiseItem;
import com.enactor.mfc.basket.items.ISalesItem;
import com.enactor.mfc.basket.items.MerchandiseItem;
import com.enactor.mfc.basket.items.ModifierType;
import com.enactor.mfc.basket.items.RoundingRule;
import com.enactor.mfc.currency.CurrencyKey;
import com.enactor.core.localisation.ILocale;
import com.enactor.core.localisation.Locale;
import com.enactor.core.localisation.MessageResources;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.annotations.Output;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.actions.DataSessionManagementType;
import com.enactor.coreUI.annotations.SessionManagement;


@Inputs({
	@Input(name = "enactor.mfc.ItemDiscountReason", type = com.enactor.mfc.reason.IItemDiscountReason.class),
	@Input(name = "enactor.mfc.DiscountItem", type = com.enactor.mfc.basket.items.IDiscountItem.class, required = true),
	@Input(name = "enactor.mfc.DiscountableItem", type = com.enactor.mfc.basket.items.IDiscountableItem.class),
	@Input(name = "enactor.coreUI.UserLocale", type = com.enactor.core.localisation.ILocale.class)
})
@Outputs({
	@Output(name = "enactor.pos.Price", type = java.lang.Long.class),
	@Output(name = "enactor.mfc.DiscountItem", type = com.enactor.mfc.basket.items.IDiscountItem.class),
	@Output(name = "enactor.pos.Percentage", type = java.lang.Float.class)
})
@Outcomes({
	"Fail", "enactor.action.PercentageEntryRequired", "enactor.action.PercentageEntryNotRequired", "enactor.action.AmountEntryRequired", "enactor.action.AmountEntryNotRequired", "enactor.action.NewPriceEntryRequired", "enactor.action.NewPriceEntryNotRequired"
})
public class ItemTaxExemptAction implements IUIAction {

	/*******************************************************
	 * Constants
	 ******************************************************/
	
	/** Default Serial Version */
	private static final long serialVersionUID = 1L;
	/*******************************************************
	 * Properties
	 ******************************************************/
	
	/** Logger */
	@SuppressWarnings("unused")private static Logger logger = Logger.getLogger(ItemTaxExemptAction.class.getName());

	/*******************************************************
	 * IUIAction Interface
	 ******************************************************/
	
	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
	throws UIProcessException, ApplicationProcessException {
		
		 
	//  Item Discount Reason
	// This is the tax exempt reason code selected by customer  	
	ITaxOverrideReason taxOverrideReason = null;
	if (inputData.containsNotNullData("enactor.mfc.TaxOverrideReason")) {
		taxOverrideReason = (ITaxOverrideReason) inputData.getData("enactor.mfc.TaxOverrideReason");
	}
	
	//  Discount Item
	//  This is tax exempt item modifer   
	IDiscountItem discountItem = null;
	if (inputData.containsNotNullData("enactor.mfc.DiscountItem")) {
	discountItem = (IDiscountItem)inputData.getData("enactor.mfc.DiscountItem");
	} else {
	throw new UIProcessException(UIProcessException.MISSING_DATA, "The variable 'enactor.mfc.DiscountItem' is required as an input");
	}
	
	
	// Discountable Item
	// This is item to be tax exempted 
	IDiscountableItem discountableItem = null;
	if (inputData.containsNotNullData("enactor.mfc.DiscountableItem")) {
	discountableItem = (IDiscountableItem)inputData.getData("enactor.mfc.DiscountableItem");
	}
	
	// User Locale
	ILocale userLocale = null;
	if (inputData.containsNotNullData("enactor.coreUI.UserLocale")) {
	userLocale = (ILocale)inputData.getData("enactor.coreUI.UserLocale");
	}
	
	IRetailTransactionHandler transactionHandler = null;
	NormalRetailSaleTransaction rsTransaction = null;;
	if (inputData.containsNotNullData("enactor.mfc.TransactionHandler")) {
		transactionHandler = (IRetailTransactionHandler)inputData.getData("enactor.mfc.TransactionHandler");
		 rsTransaction = (NormalRetailSaleTransaction)transactionHandler.getTransaction();
	}
	
	try
	{
		int basketCount = rsTransaction.getBasket().getItemCount();
		
		IMerchandiseItem item;
		MerchandiseItem merchItem;
		ISalesItem salesItem;
		String prodcutId;
		
		IMerchandiseItem discountableMerchItem =  (IMerchandiseItem) discountableItem ;
		
		String discountableProdcutId  = discountableMerchItem.getProductID();
		
		long taxAmount =0;
		
		// loop through the basket to find which item we have to tax exempt and find the tax amount which we have to deduct ..
		if (basketCount > 0)
		{
	        for (int i=1;i<=basketCount;i++)
	        {
	        	if ((rsTransaction.getBasket().getItem(i) instanceof IMerchandiseItem)) {
	        	
	        		item = (IMerchandiseItem) rsTransaction.getBasket().getItem(i);
	        		try {
	        			
	        			prodcutId = item.getProductID();
	        			if (prodcutId.equals(discountableProdcutId))
	        			{
	        				taxAmount=  item.getTaxAmount();
	        				break;
	        			}
	        			
	        		} catch (Exception e) {
	        			e.printStackTrace();
	        			return CoreUIOutcomes.FAIL_OUTCOME;
	        		}
	   		 
	        	}
	        }
	  }
		
	// modify the properties of tax modifer item ..description .. tax amount etc ..	
	 discountItem.setDescription("TAX-OVERRIDE");
	 discountItem.setType(ModifierType.PERCENTAGE);
	 if (taxOverrideReason != null) 
	    discountItem.setReasonDescription(taxOverrideReason.getDescription().getString(userLocale));
	 discountItem.setType(ModifierType.VALUE);
     discountItem.setRoundingRule(RoundingRule.UP);
	 discountItem.setReasonKey((IReasonKey)taxOverrideReason.getKey());
	 discountItem.setModifiedItem(discountableItem);
	 
	 discountItem.setValue(-taxAmount);
	 discountItem.setNetValue(-taxAmount);
	 
	 
	 outputData.setData(BasketDataTypes.DISCOUNT_ITEM_DATA, discountItem);
	 IApplicationProcessDataType retailTransHand = new ApplicationProcessDataType("enactor.mfc.TransactionHandler", IRetailTransactionHandler.class.getName());
		
	 outputData.setData(retailTransHand, transactionHandler);
		
	 
	}
	catch(Exception e)
	{
		System.out.println("Exception  " +e);
		e.printStackTrace();
	}
	
	return CoreUIOutcomes.SUCCESS_OUTCOME;
	}

}
	


