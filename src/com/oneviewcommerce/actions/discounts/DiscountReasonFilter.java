package com.oneviewcommerce.actions.discounts;

import java.util.Iterator;
import java.util.List;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.database.DataAccessContext;
import com.enactor.core.database.DatabaseException;
import com.enactor.core.database.IDataAccessSession;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.annotations.Output;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.reason.IDiscountReason;
import com.enactor.mfc.reason.IReasonKey;
import com.enactor.mfc.reason.ItemDiscountReason;
import com.enactor.mfc.reason.ReasonDBServer;
import com.enactor.mfc.reason.TransactionDiscountReason;

@Inputs( { @Input(name = "enactor.coreUI.List", type = java.util.List.class) })
@Outputs( { @Output(name = "enactor.coreUI.List", type = java.util.List.class) })
@Outcomes( { "Success" })
public class DiscountReasonFilter implements IUIAction {

	/*******************************************************
	 * Constants
	 ******************************************************/

	/** Default Serial Version */
	private static final long serialVersionUID = 1L;
	/*******************************************************
	 * Properties
	 ******************************************************/

	/** Logger */
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(DiscountReasonFilter.class.getName());
	private IDataAccessSession session = null;

	/*******************************************************
	 * IUIAction Interface
	 ******************************************************/

	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		try {
			// List
			List list = null;
			List filteredList = new java.util.ArrayList();
			if (inputData.containsNotNullData("enactor.coreUI.List")) {
				list = (List) inputData.getData("enactor.coreUI.List");
				Iterator listIterator = null;
				if (list == null || list.iterator() == null)
					return CoreUIOutcomes.SUCCESS_OUTCOME;

				listIterator = list.iterator();

				if (DataAccessContext.isSessionOpen()) {
					session = DataAccessContext.getCurrentSession();
				} else {
					session = DataAccessContext.openSession();
				}

				ReasonDBServer reasonDBServer = new ReasonDBServer();
				String discountTypeFilter = (String) inputData.getData("enactor.pos.DiscountTypeFilter");

				while (listIterator.hasNext()) {
					IDiscountReason discountReason = null;

					com.enactor.core.database.list.ListElement listElement = (com.enactor.core.database.list.ListElement) listIterator
							.next();
					discountReason = (IDiscountReason) reasonDBServer.load((IReasonKey) listElement.getKey(), null);

					if (discountReason instanceof ItemDiscountReason) {
						if (((ItemDiscountReason) discountReason).getDiscountType() != null
								&& (((ItemDiscountReason) discountReason).getDiscountType()).toString().equals(discountTypeFilter))
							filteredList.add(listElement);
					} else if (discountReason instanceof TransactionDiscountReason) {
						if (((TransactionDiscountReason) discountReason).getDiscountType() != null
								&& (((TransactionDiscountReason) discountReason).getDiscountType()).toString().equals(discountTypeFilter))
							filteredList.add(listElement);
					}
				}

				outputData.put("enactor.coreUI.List", filteredList);
			}
		} catch (DatabaseException dbe) {
			throw new ApplicationProcessException(dbe);
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (DatabaseException e) {
					e.printStackTrace();
				}
			}
		}

		return CoreUIOutcomes.SUCCESS_OUTCOME;

	}
}
