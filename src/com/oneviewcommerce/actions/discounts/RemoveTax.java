package com.oneviewcommerce.actions.discounts;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.core.transaction.ITransaction;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.annotations.Output;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.actions.DataSessionManagementType;
import com.enactor.coreUI.annotations.SessionManagement;

@Inputs({
	@Input(name = "enactor.mfc.Transaction", type = com.enactor.core.transaction.ITransaction.class)
})
@Outputs({
	})
@Outcomes({
	"Fail", "Success"
})
public class RemoveTax implements IUIAction {

	/*******************************************************
	 * Constants
	 ******************************************************/
	
	/** Default Serial Version */
	private static final long serialVersionUID = 1L;
	/*******************************************************
	 * Properties
	 ******************************************************/
	
	/** Logger */
	@SuppressWarnings("unused")private static Logger logger = Logger.getLogger(RemoveTax.class.getName());

	/*******************************************************
	 * IUIAction Interface
	 ******************************************************/
	
	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
	throws UIProcessException, ApplicationProcessException {
	
	// Transaction
	ITransaction transaction = null;
	if (inputData.containsNotNullData("enactor.mfc.Transaction")) {
		
		
		
	transaction = (ITransaction)inputData.getData("enactor.mfc.Transaction");
	
	/*
	 *  Core logic 
	 * 
	 */
	
	}
	
	return CoreUIOutcomes.SUCCESS_OUTCOME;
	
	}

}
