package com.oneviewcommerce.actions.discounts;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.mfc.customer.ICustomer;
import com.enactor.mfc.retail.transaction.INormalRetailSaleTransaction;
import com.enactor.mfc.retail.transaction.IRetailTransactionHandler;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.annotations.Output;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.actions.DataSessionManagementType;
import com.enactor.coreUI.annotations.SessionManagement;

@Inputs({
	@Input(name = "enactor.mfc.TransactionHandler", type = com.enactor.mfc.retail.transaction.IRetailTransactionHandler.class, required = true)
})
@Outputs({
	})
@Outcomes({
	"Success", "Fail"
})
public class IsCustomerTaxExempt implements IUIAction {

	/*******************************************************
	 * Constants
	 ******************************************************/
	
	/** Default Serial Version */
	private static final long serialVersionUID = 1L;
	/*******************************************************
	 * Properties
	 ******************************************************/
	
	/** Logger */
	@SuppressWarnings("unused")private static Logger logger = Logger.getLogger(IsCustomerTaxExempt.class.getName());

	/*******************************************************
	 * IUIAction Interface
	 ******************************************************/
	
	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
	throws UIProcessException, ApplicationProcessException {
	
	// Transaction Handler
	IRetailTransactionHandler transactionHandler = null;
	if (inputData.containsNotNullData("enactor.mfc.TransactionHandler")) {
	transactionHandler = (IRetailTransactionHandler)inputData.getData("enactor.mfc.TransactionHandler");
	
	return CoreUIOutcomes.FAIL_OUTCOME;
/*
	INormalRetailSaleTransaction basket = (INormalRetailSaleTransaction)transactionHandler.getTransaction();
	basket.getCustomerRetailDetails().getCustomerNumber()
	
	transactionHandler.transaction.customerRetailDetails.customerNumber
	
	//CustomerNotes cstNotes = new CustomerNotes();
	
	String taxIdentification = "";
	if (cust != null)
		taxIdentification = cust.getTaxIdentification();
	
	System.out.println(" taxIdentification" + taxIdentification);
	if (taxIdentification != null & taxIdentification.equals("EXEMPT"))
		return CoreUIOutcomes.SUCCESS_OUTCOME;
	else 
		return CoreUIOutcomes.FAIL_OUTCOME;
		*/
	
	
	} else {
	throw new UIProcessException(UIProcessException.MISSING_DATA, "The variable 'enactor.mfc.TransactionHandler' is required as an input");
	
	}
	
	
	}

}
