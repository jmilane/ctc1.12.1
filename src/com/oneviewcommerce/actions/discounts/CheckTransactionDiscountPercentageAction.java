package com.oneviewcommerce.actions.discounts;

import java.util.List;

import com.enactor.core.application.process.ApplicationProcessDataType;
import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.ApplicationProcessOutcome;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessDataType;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.entities.PropertyValidationException;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.annotations.Output;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.BasketDataTypes;
import com.enactor.mfc.basket.items.IModifiableItem;
import com.enactor.mfc.basket.items.ITaxableItem;
import com.enactor.mfc.basket.items.ITransactionDiscountItem;
import com.enactor.pos.packages.discounts.processes.TestDiscountItem;
import com.enactor.pos.processes.PosActionFunctions;
import com.enactor.pos.processes.PosDataTypes;

/**
 * Validate discount percentage against reason.
 * 
 * @author MSC
 */
@Inputs( { @Input(name = BasketDataTypes.TRANSACTION_DISCOUNT_ITEM_DATA_NAME, type = ITransactionDiscountItem.class),
		@Input(name = PosDataTypes.PERCENTAGE_DATA_NAME, type = Float.class) })
@Outputs( { @Output(name = "enactor.pos.HighestDiscountPercentage", type = Float.class) })
@Outcomes( { CoreUIOutcomes.SUCCESS_OUTCOME_NAME, "DiscountPercentageRequired", "DiscountPercentageTooHigh" })
public class CheckTransactionDiscountPercentageAction extends TestDiscountItem {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	public final static IApplicationProcessOutcome DISCOUNT_PERCENTAGE_REQUIRED = new ApplicationProcessOutcome(
			"DiscountPercentageRequired");

	public final static IApplicationProcessOutcome DISCOUNT_PERCENTAGE_TOO_HIGH = new ApplicationProcessOutcome("DiscountPercentageTooHigh");

	public final static IApplicationProcessOutcome DISCOUNT_PERCENTAGE_ZERO = new ApplicationProcessOutcome("DiscountPercentageZero");

	public final static IApplicationProcessDataType HIGHEST_DISCOUNT_PERCENTAGE_DATA = new ApplicationProcessDataType(
			"enactor.pos.HighestDiscountPercentage", Float.class.getName());

	/***************************************************************************
	 * IUIAction Interface
	 **************************************************************************/

	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws ApplicationProcessException, UIProcessException {

		ITransactionDiscountItem discountItem = PosActionFunctions.getTransactionDiscountItem(inputData);

		try {
			long netValue = 0;
			long taxAdjustment = 0;
			List<IModifiableItem> items = discountItem.getModifiedItems();
			for (IModifiableItem modifiableItem : items) {
				netValue += modifiableItem.getEffectiveNetValue();
				taxAdjustment += ((ITaxableItem) modifiableItem).getTaxAdjustmentAmount();
			}
			netValue = Math.abs(netValue);
			float percentage = 0;
			if (inputData.containsNotNullData(PosDataTypes.PERCENTAGE_DATA)) {
				percentage = (Float) inputData.getData(PosDataTypes.PERCENTAGE_DATA);
			} else {
				return DISCOUNT_PERCENTAGE_REQUIRED;
			}
			if (percentage == 0f) {
				return DISCOUNT_PERCENTAGE_ZERO;
			}
			float maxPercentage = discountItem.getMaxDiscountPercentage();
			if (maxPercentage == 0f) {
				// 0% from the reason code
				// assume no reason were set, use 100%
				maxPercentage = 1.0f;
			}
			if (discountItem.getDiscountAmountLimit() != 0 && netValue != 0) {
				/*
				 * unlike item discount, the amount limit for transaction
				 * discount is not a per item value
				 */
				double amountLimitPercentage = (double) discountItem.getDiscountAmountLimit() / (double) netValue;
				// round to 2 decimal (note 10% is stored as 0.1 therefore the
				// rounding needs to be 4 decimal places)
				amountLimitPercentage = Math.floor(amountLimitPercentage * 10000f) / 10000f;
				if (amountLimitPercentage < maxPercentage) {
					maxPercentage = (float) amountLimitPercentage;
				}

			}
			if (taxAdjustment != 0 && netValue != 0) {
				double taxAdjustPercentage = (double) (netValue - taxAdjustment) / (double) netValue;
				taxAdjustPercentage = Math.floor(taxAdjustPercentage * 10000f) / 10000f;
				if (taxAdjustPercentage < maxPercentage) {
					maxPercentage = (float) taxAdjustPercentage;
				}
			}

			discountItem.setPercentage(percentage);

			if (percentage > maxPercentage) {
				outputData.setData(HIGHEST_DISCOUNT_PERCENTAGE_DATA, maxPercentage);
				return DISCOUNT_PERCENTAGE_TOO_HIGH;
			}

		} catch (PropertyValidationException e) {
			throw new UIProcessException(e, UIProcessException.INVALID_DATA, "Error setting discount item properties.");
		}

		return CoreUIOutcomes.SUCCESS_OUTCOME;

	}

}
