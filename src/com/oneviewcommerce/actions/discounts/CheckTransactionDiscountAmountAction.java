package com.oneviewcommerce.actions.discounts;

import java.util.List;

import com.enactor.core.application.process.ApplicationProcessDataType;
import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.ApplicationProcessOutcome;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessDataType;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.entities.PropertyValidationException;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.annotations.Output;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.BasketDataTypes;
import com.enactor.mfc.basket.items.IModifiableItem;
import com.enactor.mfc.basket.items.ITaxableItem;
import com.enactor.mfc.basket.items.ITransactionDiscountItem;
import com.enactor.pos.packages.discounts.processes.TestDiscountItem;
import com.enactor.pos.processes.PosActionFunctions;
import com.enactor.pos.processes.PosDataTypes;

/**
 * Validate discount amount against reason.
 * 
 * @author MSC
 */
@Inputs( { @Input(name = BasketDataTypes.TRANSACTION_DISCOUNT_ITEM_DATA_NAME, type = ITransactionDiscountItem.class),
		@Input(name = PosDataTypes.PRICE_DATA_NAME, type = Long.class) })
@Outputs( { @Output(name = "enactor.pos.HighestDiscountAmount", type = Long.class) })
@Outcomes( { CoreUIOutcomes.SUCCESS_OUTCOME_NAME, "DiscountAmountRequired", "DiscountAmountTooHigh" })
public class CheckTransactionDiscountAmountAction extends TestDiscountItem {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	public final static IApplicationProcessOutcome DISCOUNT_AMOUNT_REQUIRED = new ApplicationProcessOutcome("DiscountAmountRequired");
	public final static IApplicationProcessOutcome DISCOUNT_AMOUNT_TOO_HIGH = new ApplicationProcessOutcome("DiscountAmountTooHigh");
	public final static IApplicationProcessOutcome INVALID_DISCOUNT_AMOUNT = new ApplicationProcessOutcome("InvalidDiscountAmount");

	public final static IApplicationProcessDataType HIGHEST_DISCOUNT_AMOUNT_DATA = new ApplicationProcessDataType(
			"enactor.pos.HighestDiscountAmount", Long.class.getName());

	/***************************************************************************
	 * IUIAction Interface
	 **************************************************************************/

	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws ApplicationProcessException, UIProcessException {

		ITransactionDiscountItem discountItem = PosActionFunctions.getTransactionDiscountItem(inputData);

		try {
			float maxDiscountPercentage = 0;
			long netValue = 0;
			long netValueWithoutTaxes = 0;
			List<IModifiableItem> items = discountItem.getModifiedItems();
			for (IModifiableItem modifiableItem : items) {
				netValue += modifiableItem.getEffectiveNetValue();
				netValueWithoutTaxes += modifiableItem.getEffectiveNetValue();
				if (modifiableItem instanceof ITaxableItem) {
					netValue += ((ITaxableItem) modifiableItem).getTaxAdjustmentAmount();
				}
			}
			long oriNetValue = netValue;
			netValue = Math.abs(netValue);
			long value = 0;
			if (inputData.containsNotNullData(PosDataTypes.PRICE_DATA)) {
				value = (Long) inputData.getData(PosDataTypes.PRICE_DATA);
			} else {
				return DISCOUNT_AMOUNT_REQUIRED;
			}
			maxDiscountPercentage = discountItem.getMaxDiscountPercentage();
			long highestValue = netValue;
			if (maxDiscountPercentage != 0) {
				long highValue = (long) (maxDiscountPercentage * (float) netValue);
				if (highValue < highestValue) {
					highestValue = highValue;
				}
			}
			if (discountItem.getDiscountAmountLimit() != 0) {
				if (discountItem.getDiscountAmountLimit() < highestValue) {
					highestValue = discountItem.getDiscountAmountLimit();
				}
			}

			if (oriNetValue < 0) {
				discountItem.setValue(-value);
			} else {
				discountItem.setValue(value);
			}
			if (value >= netValueWithoutTaxes)
				return INVALID_DISCOUNT_AMOUNT;
			else if (value > highestValue) {
				outputData.setData(HIGHEST_DISCOUNT_AMOUNT_DATA, highestValue);
				return DISCOUNT_AMOUNT_TOO_HIGH;
			}

		} catch (PropertyValidationException e) {
			throw new UIProcessException(e, UIProcessException.INVALID_DATA, "Error setting discount item properties.");
		}

		return CoreUIOutcomes.SUCCESS_OUTCOME;

	}

}