package com.oneviewcommerce.actions.discounts;

import com.enactor.core.application.process.ApplicationProcessDataType;
import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.ApplicationProcessOutcome;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessDataType;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.entities.PropertyValidationException;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.annotations.Output;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.BasketDataTypes;
import com.enactor.mfc.basket.items.IDiscountItem;
import com.enactor.mfc.basket.items.IDiscountableItem;
import com.enactor.mfc.basket.items.ISalesItem;
import com.enactor.mfc.basket.items.ITaxableItem;
import com.enactor.pos.processes.PosActionFunctions;
import com.enactor.pos.processes.PosDataTypes;

/**
 * Validate discount percentage against reason.
 * 
 * @author MSC
 */
@Inputs( { @Input(name = BasketDataTypes.DISCOUNT_ITEM_DATA_NAME, type = IDiscountItem.class),
		@Input(name = PosDataTypes.PERCENTAGE_DATA_NAME, type = Float.class) })
@Outputs( { @Output(name = CheckPriceDiscountPercentageAction.HIGHEST_DISCOUNT_PERCENTAGE_DATA_NAME, type = Float.class) })
@Outcomes( { CoreUIOutcomes.SUCCESS_OUTCOME_NAME, CheckPriceDiscountPercentageAction.DISCOUNT_PERCENTAGE_REQUIRED_NAME,
		CheckPriceDiscountPercentageAction.DISCOUNT_PERCENTAGE_TOO_HIGH_NAME })
public class CheckPriceDiscountPercentageAction implements IUIAction {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	public static final String DISCOUNT_PERCENTAGE_REQUIRED_NAME = "DiscountPercentageRequired";
	public final static IApplicationProcessOutcome DISCOUNT_PERCENTAGE_REQUIRED = new ApplicationProcessOutcome(
			DISCOUNT_PERCENTAGE_REQUIRED_NAME);

	public static final String DISCOUNT_PERCENTAGE_TOO_HIGH_NAME = "DiscountPercentageTooHigh";
	public final static IApplicationProcessOutcome DISCOUNT_PERCENTAGE_TOO_HIGH = new ApplicationProcessOutcome(
			DISCOUNT_PERCENTAGE_TOO_HIGH_NAME);

	public static final String HIGHEST_DISCOUNT_PERCENTAGE_DATA_NAME = "enactor.pos.HighestDiscountPercentage";
	public final static IApplicationProcessDataType HIGHEST_DISCOUNT_PERCENTAGE_DATA = new ApplicationProcessDataType(
			HIGHEST_DISCOUNT_PERCENTAGE_DATA_NAME, Float.class.getName());

	public static final String DISCOUNT_PERCENTAGE_ZERO_NAME = "DiscountPercentageZero";
	public final static IApplicationProcessOutcome DISCOUNT_PERCENTAGE_ZERO = new ApplicationProcessOutcome(DISCOUNT_PERCENTAGE_ZERO_NAME);

	/***************************************************************************
	 * IUIAction Interface
	 **************************************************************************/

	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws ApplicationProcessException, UIProcessException {

		IDiscountItem discountItem = PosActionFunctions.getDiscountItem(inputData);
		IDiscountableItem discountableItem = PosActionFunctions.getDiscountableItem(inputData);

		try {
			long netValue = Math.abs(discountItem.getModifiedItem().getEffectiveNetValue());
			float percentage = 0;
			if (inputData.containsNotNullData(PosDataTypes.PERCENTAGE_DATA)) {
				percentage = (Float) inputData.getData(PosDataTypes.PERCENTAGE_DATA);
			} else {
				return DISCOUNT_PERCENTAGE_REQUIRED;
			}
			if (percentage == 0f) {
				return DISCOUNT_PERCENTAGE_ZERO;
			}
			float maxPercentage = discountItem.getMaxDiscountPercentage();
			if (discountableItem.getMaxDiscount() != 0 && (maxPercentage == 0 || discountableItem.getMaxDiscount() < maxPercentage)) {
				maxPercentage = discountableItem.getMaxDiscount();
			}
			if (maxPercentage == 0f) {
				// 0% from the reason code
				// assume no reason were set, use 100%
				maxPercentage = 1.0f;
			}
			if (discountItem.getDiscountAmountLimit() != 0 && netValue != 0) {
				double qtyAmountLimit = discountItem.getDiscountAmountLimit();
				double qty = 0;
				/*
				 * the amount limit is for one item, multiply that with the qty
				 * before getting the percentage
				 */
				if (discountItem.getModifiedItem() instanceof ISalesItem) {
					qty = ((ISalesItem) discountItem.getModifiedItem()).getQuantity();
				}
				if (qty != 0 && qty != 1) {
					qtyAmountLimit = (long) (qty * (double) qtyAmountLimit);
				}
				double amountLimitPercentage = qtyAmountLimit / netValue;
				// round to 2 decimal (note 10% is stored as 0.1 therefore the
				// rounding needs to be 4 decimal places)
				amountLimitPercentage = Math.floor(amountLimitPercentage * 10000f) / 10000f;
				if (amountLimitPercentage < maxPercentage) {
					maxPercentage = (float) amountLimitPercentage;
				}

			}
			if (percentage > maxPercentage) {
				// Set percentage in case user wants to continue
				discountItem.setPercentage(percentage);

				outputData.setData(HIGHEST_DISCOUNT_PERCENTAGE_DATA, maxPercentage);
				return DISCOUNT_PERCENTAGE_TOO_HIGH;
			}
			if (discountableItem instanceof ITaxableItem && ((ITaxableItem) discountableItem).getTaxAdjustmentAmount() != 0) {
				long remainingValue = netValue - Math.round(percentage * (float) netValue);
				if (remainingValue < ((ITaxableItem) discountableItem).getTaxAdjustmentAmount()) {
					// Set percentage in case user wants to continue
					discountItem.setPercentage(percentage);

					maxPercentage = (float) (netValue - ((ITaxableItem) discountableItem).getTaxAdjustmentAmount()) / (float) netValue;
					maxPercentage = (float) (Math.floor(maxPercentage * 10000f) / 10000f);
					outputData.setData(HIGHEST_DISCOUNT_PERCENTAGE_DATA, maxPercentage);
					return DISCOUNT_PERCENTAGE_TOO_HIGH;
				}
			}
			discountItem.setPercentage(percentage);

		} catch (PropertyValidationException e) {
			throw new UIProcessException(e, UIProcessException.INVALID_DATA, "Error setting discount item properties.");
		}

		return CoreUIOutcomes.SUCCESS_OUTCOME;

	}

}