package com.oneviewcommerce.actions.discounts;

import com.enactor.core.application.process.ApplicationProcessDataType;
import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.ApplicationProcessOutcome;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessDataType;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.entities.PropertyValidationException;
import com.enactor.coreUI.annotations.Input;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.annotations.Output;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.BasketDataTypes;
import com.enactor.mfc.basket.items.IDiscountItem;
import com.enactor.mfc.basket.items.IDiscountableItem;
import com.enactor.mfc.basket.items.ITaxableItem;
import com.enactor.pos.packages.discounts.processes.TestDiscountItem;
import com.enactor.pos.processes.PosActionFunctions;
import com.enactor.pos.processes.PosDataTypes;

/**
 * Validate discount amount against reason.
 * 
 * @author MSC
 */
@Inputs( { @Input(name = BasketDataTypes.DISCOUNT_ITEM_DATA_NAME, type = IDiscountItem.class),
		@Input(name = PosDataTypes.PRICE_DATA_NAME, type = Long.class),
		@Input(name = PosDataTypes.BUY_BACK_DATA_NAME, type = Boolean.class) })
@Outputs( { @Output(name = "enactor.pos.HighestDiscountAmount", type = Long.class) })
@Outcomes( { CoreUIOutcomes.SUCCESS_OUTCOME_NAME, "DiscountAmountRequired", "DiscountAmountTooHigh" })
public class CheckPriceDiscountAmountAction extends TestDiscountItem {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	public final static IApplicationProcessOutcome DISCOUNT_AMOUNT_REQUIRED = new ApplicationProcessOutcome("DiscountAmountRequired");

	public final static IApplicationProcessOutcome DISCOUNT_AMOUNT_TOO_HIGH = new ApplicationProcessOutcome("DiscountAmountTooHigh");

	public final static IApplicationProcessOutcome INVALID_DISCOUNT_AMOUNT = new ApplicationProcessOutcome("InvalidDiscountAmount");

	public final static IApplicationProcessDataType HIGHEST_DISCOUNT_AMOUNT_DATA = new ApplicationProcessDataType(
			"enactor.pos.HighestDiscountAmount", Long.class.getName());

	/***************************************************************************
	 * IUIAction Interface
	 **************************************************************************/

	/**
	 * {@inheritDoc}
	 */
	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws ApplicationProcessException, UIProcessException {

		IDiscountItem discountItem = PosActionFunctions.getDiscountItem(inputData);
		boolean buyBack = (Boolean) PosActionFunctions.getOptionalBooleanValue(inputData, PosDataTypes.BUY_BACK_DATA, false);

		try {
			float maxDiscountPercentage = 0;
			long netValue = Math.abs(discountItem.getModifiedItem().getEffectiveNetValue());
			long value = 0;
			if (inputData.containsNotNullData(PosDataTypes.PRICE_DATA)) {
				value = (Long) inputData.getData(PosDataTypes.PRICE_DATA);
			} else {
				return DISCOUNT_AMOUNT_REQUIRED;
			}
			maxDiscountPercentage = getMaxDiscountPercentage(discountItem);
			long highestValue = netValue;
			if (maxDiscountPercentage != 0) {
				long highValue = (long) (maxDiscountPercentage * (float) netValue);
				if (highValue < highestValue) {
					highestValue = highValue;
				}
			}
			if (discountItem.getDiscountAmountLimit() != 0) {
				if (buyBack) {
					if (highestValue == 0 || discountItem.getDiscountAmountLimit() < highestValue) {
						highestValue = discountItem.getDiscountAmountLimit();
					}
				} else {
					if (discountItem.getDiscountAmountLimit() < highestValue) {
						highestValue = discountItem.getDiscountAmountLimit();
					}
				}
			}
			IDiscountableItem discountableItem = discountItem.getModifiedItem();
			if (discountableItem instanceof ITaxableItem && ((ITaxableItem) discountableItem).getTaxAdjustmentAmount() != 0) {
				long remainingNetValue = netValue - ((ITaxableItem) discountableItem).getTaxAdjustmentAmount();
				if (remainingNetValue > 0) {
					if (buyBack) {
						if (highestValue == 0 || remainingNetValue < highestValue) {
							highestValue = remainingNetValue;
						}
					} else {
						if (remainingNetValue < highestValue) {
							highestValue = remainingNetValue;
						}
					}
				}
			}
			if (buyBack && highestValue == 0) {
				// buy back items have 0 value
				// therefore if buy back and the limits are not set then
				// don't test the value against the highest
			} else {
				if (value > discountableItem.getEffectiveNetValue())
					return INVALID_DISCOUNT_AMOUNT;
				else if (value > highestValue) {
					// set the discount item value in case the user decides to continue
					discountItem.setValue(value);

					outputData.setData(HIGHEST_DISCOUNT_AMOUNT_DATA, highestValue);
					return DISCOUNT_AMOUNT_TOO_HIGH;
				}
			}
			discountItem.setValue(value);

		} catch (PropertyValidationException e) {
			throw new UIProcessException(e, UIProcessException.INVALID_DATA, "Error setting discount item properties.");
		}

		return CoreUIOutcomes.SUCCESS_OUTCOME;

	}

	/**
	 * get the lower max discount percentage from either the discount item or the modified item, ignore zero percentage
	 * 
	 * @param discountItem
	 * @return max discount percentage
	 */
	private float getMaxDiscountPercentage(IDiscountItem discountItem) {
		IDiscountableItem discountableItem = discountItem.getModifiedItem();
		if (discountableItem.getMaxDiscount() == 0) {
			return discountItem.getMaxDiscountPercentage();
		} else if (discountItem.getMaxDiscountPercentage() == 0) {
			return discountableItem.getMaxDiscount();
		} else {
			if (discountableItem.getMaxDiscount() < discountItem.getMaxDiscountPercentage()) {
				return discountableItem.getMaxDiscount();
			} else {
				return discountItem.getMaxDiscountPercentage();
			}
		}
	}

}
