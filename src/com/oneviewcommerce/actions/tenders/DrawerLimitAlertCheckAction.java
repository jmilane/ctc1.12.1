package com.oneviewcommerce.actions.tenders;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import com.enactor.cashManagement.CashManagementConstants;
import com.enactor.cashManagement.processes.IDrawerLimitStatus;
import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.application.process.NoApplicationDataElementException;
import com.enactor.core.database.DatabaseException;
import com.enactor.core.factories.UnknownServerException;
import com.enactor.core.localisation.ILocale;
import com.enactor.core.servers.EntityServerProxy;
import com.enactor.core.servers.LockType;
import com.enactor.core.utilities.Logger;
import com.enactor.core.utilities.StringUtils;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.actions.UIActionFunctions;
import com.enactor.coreUI.processes.CoreUIDataTypes;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.tender.ITender;
import com.enactor.mfc.tender.ITenderKey;
import com.enactor.mfc.tender.ITenderServer;
import com.enactor.mfc.tender.Tender;

public class DrawerLimitAlertCheckAction implements IUIAction {

	public DrawerLimitAlertCheckAction() {
	}

	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {
		List<ITenderKey> countAlertTenderKeyList = new ArrayList<ITenderKey>();
		String alertTenderDescriptions = "";
		IDrawerLimitStatus drawerLimitStatus = (IDrawerLimitStatus) UIActionFunctions.getRequiredDataItem(inputData,
				CashManagementConstants.DRAWER_LIMIT_STATUS_DATA);
		int maxWarningCount = 3;
		if (inputData.containsNotNullData(CashManagementConstants.MAX_WARNING_COUNT_DATA))
			maxWarningCount = ((Integer) inputData.getData(CashManagementConstants.MAX_WARNING_COUNT_DATA)).intValue();
		Locale locale = null;
		try {
			ILocale userLocale = (ILocale) inputData.getData(CoreUIDataTypes.USER_LOCALE_DATA);
			locale = userLocale.getLocale();
		} catch (NoApplicationDataElementException e) {
		}
		ITenderServer tenderServer;
		try {
			tenderServer = (ITenderServer) EntityServerProxy.getServer(Tender.ENTITY_QNAME);
		} catch (UnknownServerException e) {
			logger.log(1000,
					(new StringBuilder()).append("Failed to get tender server ").append(Tender.ENTITY_QNAME.toString()).toString(), e);
			return CoreUIOutcomes.FAIL_OUTCOME;
		}
		IApplicationProcessOutcome outcome = CoreUIOutcomes.EMPTY_OUTCOME;
		synchronized (drawerLimitStatus) {
			Date lastAlertTime = drawerLimitStatus.getLastAlertTime();
			Date lastCheckTime = drawerLimitStatus.getLastLimitCheckTime();

			Iterator<ITenderKey> i$ = drawerLimitStatus.getOverLimitTenderList().iterator();
			do {
				if (!i$.hasNext())
					break;
				ITenderKey tenderKey = (ITenderKey) i$.next();
				String description = null;
				try {
					ITender tender = (ITender) tenderServer.load(tenderKey, LockType.READ_LOCK);
					description = tender.getDescription().getString(locale);
				} catch (DatabaseException e) {
					logger.log(600, (new StringBuilder()).append("tender not defined ").append(tenderKey.toString()).toString(), e);
				}
				if (StringUtils.isEmpty(description))
					description = tenderKey.getTenderId();
				if (alertTenderDescriptions.length() != 0)
					alertTenderDescriptions = (new StringBuilder()).append(alertTenderDescriptions).append(", ").toString();
				alertTenderDescriptions = (new StringBuilder()).append(alertTenderDescriptions).append(description).toString();
				int count = drawerLimitStatus.getWarningCountMap().get(tenderKey) != null ? ((Integer) drawerLimitStatus
						.getWarningCountMap().get(tenderKey)).intValue() : 0;
				count++;
				if (maxWarningCount > 0) {
					if (count >= maxWarningCount + 1) {
						countAlertTenderKeyList.add(tenderKey);
						count = 1;
					}
					drawerLimitStatus.getWarningCountMap().put(tenderKey, Integer.valueOf(count));
				}
			} while (true);
			if (drawerLimitStatus.getOverLimitTenderList().size() > 0) {
				drawerLimitStatus.setLastAlertTime(drawerLimitStatus.getLastLimitCheckTime());
				outcome = CoreUIOutcomes.SUCCESS_OUTCOME;
			}
		}
		outputData.setData(CashManagementConstants.DRAWER_LIMIT_STATUS_DATA, drawerLimitStatus);
		outputData.setData(CashManagementConstants.EXCEED_WARNING_COUNT_TENDER_KEY_LIST_DATA, countAlertTenderKeyList);
		outputData.setData(CashManagementConstants.ALERT_TENDER_DESCRIPTIONS_DATA, alertTenderDescriptions);
		return outcome;
	}

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(DrawerLimitAlertCheckAction.class.getName());

}
