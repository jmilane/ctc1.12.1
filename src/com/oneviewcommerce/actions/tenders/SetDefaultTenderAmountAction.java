package com.oneviewcommerce.actions.tenders;

import java.util.ArrayList;

import com.enactor.cashManagement.listElements.TenderListElement;
import com.enactor.core.application.process.ApplicationProcessDataType;
import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessDataType;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.actions.UIActionFunctions;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;

public class SetDefaultTenderAmountAction implements IUIAction {

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(SetDefaultTenderAmountAction.class.getName());

	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		Long tenderAmount = (Long) UIActionFunctions.getRequiredDataItem(inputData, "enactor.pos.TenderAmount");
		ArrayList<TenderListElement> tendersList = (ArrayList<TenderListElement>) UIActionFunctions.getRequiredDataItem(inputData,
				"enactor.cashManagement.TendersList");
		for (int i = 0; i < tendersList.size(); ++i) {
			TenderListElement tle = tendersList.get(i);
			tle.setTotal(tenderAmount);
		}

		IApplicationProcessDataType defaultList = new ApplicationProcessDataType("enactor.cashManagement.TendersList", java.util.List.class
				.getName());
		outputData.setData(defaultList, tendersList);
		return CoreUIOutcomes.SUCCESS_OUTCOME;

	}
}
