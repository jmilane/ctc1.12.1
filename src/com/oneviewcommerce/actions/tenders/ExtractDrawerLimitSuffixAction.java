package com.oneviewcommerce.actions.tenders;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessDataExpression;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.el.ApplicationProcessELEvaluator;
import com.enactor.core.entities.IEntityList;
import com.enactor.core.localisation.ILocale;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.menu.IMenu;
import com.enactor.mfc.menu.IMenuElement;
import com.enactor.mfc.menu.IMenuItem;

public class ExtractDrawerLimitSuffixAction implements IUIAction {

	private static final long serialVersionUID = 1L;

	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		final String suffixVarName = "ctc.pos.DrawerLimitSuffix";
		String suffixValue = "";

		if (inputData.containsNotNullData("enactor.mfc.menu.Menu")) {
			final IMenu menu = (IMenu) inputData.getData("enactor.mfc.menu.Menu");
			IMenuElement cashElement = null;
			final IEntityList<IMenuElement> menuElements = menu.getSubMenuList();
			int i = 0;
			while (i < menuElements.size() && null == cashElement) {
				IMenuElement menuElement = menuElements.get(i++);
				if (menuElement.getLabel().equals("Cash")) {
					cashElement = menuElement;
				}
			}
			if (null != cashElement) {
				IApplicationProcessDataExpression[] expressions = ((IMenuItem) cashElement).getDataExpressions();
				ApplicationProcessELEvaluator evaluator = new ApplicationProcessELEvaluator();
				i = 0;
				while (i < expressions.length && suffixValue.isEmpty()) {
					final IApplicationProcessDataExpression expr = expressions[i++];
					if (expr.getName().equals(suffixVarName)) {
						String value = (String) evaluator.evaluateVariableOrExpression(expr.getExpression(), String.class.getName());
						suffixValue = value;
					}
				}
			}
		}

		if (suffixValue.isEmpty()) {
			if (inputData.containsNotNullData("enactor.coreUI.UserLocale")) {
				ILocale userLocale = (ILocale) inputData.getData("enactor.coreUI.UserLocale");
				if (userLocale.getLanguage().equals("fr")) {
					suffixValue = "tr�sorerie";
				} else {
					// default to English
					suffixValue = "cash";
				}
			}
		}

		outputData.putStringData(suffixVarName, suffixValue);
		return CoreUIOutcomes.SUCCESS_OUTCOME;
	}
}
