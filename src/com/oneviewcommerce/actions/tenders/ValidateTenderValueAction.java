package com.oneviewcommerce.actions.tenders;

import java.util.Locale;

import com.enactor.core.application.process.ApplicationProcessDataType;
import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.ApplicationProcessOutcome;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessDataType;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.utilities.StringUtils;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.actions.UIActionFunctions;
import com.enactor.coreUI.annotations.Inputs;
import com.enactor.coreUI.annotations.Outcomes;
import com.enactor.coreUI.annotations.Outputs;
import com.enactor.coreUI.factories.DataParserFactory;
import com.enactor.coreUI.format.DataParseException;
import com.enactor.coreUI.format.IDataParser;
import com.enactor.coreUI.processes.CoreUIDataTypes;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.currency.CurrencyDataTypes;
import com.enactor.pos.processes.PosDataTypes;

@Inputs({
		@com.enactor.coreUI.annotations.Input(name = "enactor.coreUI.Locale", type = Locale.class, required = true),
		@com.enactor.coreUI.annotations.Input(name = "enactor.mfc.CurrencyId", type = String.class, required = true),
		@com.enactor.coreUI.annotations.Input(name = "enactor.pos.TenderAmount", type = String.class),
		@com.enactor.coreUI.annotations.Input(name = "enactor.pos.MaxValue", type = Long.class),
		@com.enactor.coreUI.annotations.Input(name = "enactor.pos.AllowNegativeValue", type = Boolean.class) })
@Outputs({ @com.enactor.coreUI.annotations.Output(name = "enactor.pos.TenderValue", type = Long.class) })
@Outcomes({ "ValueRequired", "ValueInvalid", "ValueTooBig", "Success" })
public class ValidateTenderValueAction implements IUIAction {
	private static final long serialVersionUID = 1L;
	public static final String ALLOW_NEGATIVE_VALUE_DATA_NAME = "enactor.pos.AllowNegativeValue";
	public static final IApplicationProcessDataType ALLOW_NEGATIVE_VALUE_DATA = new ApplicationProcessDataType(
			"enactor.pos.AllowNegativeValue", Boolean.class.getName());
	public static final String MAX_VALUE_DATA_NAME = "enactor.pos.MaxValue";
	public static final IApplicationProcessDataType MAX_VALUE_DATA = new ApplicationProcessDataType(
			"enactor.pos.MaxValue", Long.class.getName());
	public static final String TENDER_VALUE_DATA_NAME = "enactor.pos.TenderValue";
	public static final IApplicationProcessDataType TENDER_VALUE_DATA = new ApplicationProcessDataType(
			"enactor.pos.TenderValue", Long.class.getName());
	public static final String VALUE_INVALID_OUTCOME_NAME = "ValueInvalid";
	public static final IApplicationProcessOutcome VALUE_INVALID_OUTCOME = new ApplicationProcessOutcome(
			"ValueInvalid");
	public static final String VALUE_REQUIRED_OUTCOME_NAME = "ValueRequired";
	public static final IApplicationProcessOutcome VALUE_REQUIRED_OUTCOME = new ApplicationProcessOutcome(
			"ValueRequired");
	public static final String VALUE_TOO_BIG_OUTCOME_NAME = "ValueTooBig";
	public static final IApplicationProcessOutcome VALUE_TOO_BIG_OUTCOME = new ApplicationProcessOutcome(
			"ValueTooBig");

	public IApplicationProcessOutcome execute(
			IApplicationProcessData inputData,
			IApplicationProcessData outputData)
			throws ApplicationProcessException, UIProcessException {
		String currencyId = (String) UIActionFunctions.getRequiredDataItem(
				inputData, CurrencyDataTypes.CURRENCY_ID_DATA);
		Locale locale = (Locale) UIActionFunctions.getRequiredDataItem(
				inputData, CoreUIDataTypes.LOCALE_DATA);
		String tenderAmountStr = ((Long) UIActionFunctions
				.getOptionalDataItem(inputData, PosDataTypes.TENDER_AMOUNT_DATA)).toString();
		Long maxValue = (Long) UIActionFunctions.getOptionalDataItem(inputData,
				MAX_VALUE_DATA);
		Boolean allowNegativeValue = (Boolean) UIActionFunctions
				.getOptionalDataItem(inputData, ALLOW_NEGATIVE_VALUE_DATA,
						Boolean.valueOf(false));

		if (StringUtils.isEmpty(tenderAmountStr))
			return VALUE_REQUIRED_OUTCOME;
		try {
			IDataParser dataParser = DataParserFactory.getInstance().getParser(
					locale, currencyId);
			long value = dataParser.parseAmount(tenderAmountStr, false);

			if ((!(allowNegativeValue.booleanValue())) && (value < 0L)) {
				return VALUE_INVALID_OUTCOME;
			}
			if ((maxValue != null) && (value > maxValue.longValue())) {
				return VALUE_TOO_BIG_OUTCOME;
			}
			outputData.setData(TENDER_VALUE_DATA, Long.valueOf(value/100));

			return CoreUIOutcomes.SUCCESS_OUTCOME;
		} catch (DataParseException e) {
		}
		return VALUE_INVALID_OUTCOME;
	}
}
