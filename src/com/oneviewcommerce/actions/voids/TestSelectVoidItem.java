package com.oneviewcommerce.actions.voids;

import com.enactor.mfc.basket.IBasket;
import com.enactor.mfc.basket.items.IBasketItem;
import com.enactor.mfc.basket.items.IDispatchDetailsItem;

// Referenced classes of package com.enactor.pos.packages.voids.processes:
//            TestVoidItem

public class TestSelectVoidItem extends TestVoidItem {

	public TestSelectVoidItem() {
	}

	protected boolean testVoidable(IBasket basket, IBasketItem basketItem, boolean voidAsChild) {
		return super.testVoidable(basket, basketItem, voidAsChild) && !(basketItem instanceof IDispatchDetailsItem);
	}

	private static final long serialVersionUID = 1L;
}
