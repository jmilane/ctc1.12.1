package com.oneviewcommerce.actions.voids;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.ApplicationProcessOutcome;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.BasketDataTypes;
import com.enactor.mfc.basket.IBasket;
import com.enactor.mfc.basket.items.IAccountPaymentItem;
import com.enactor.mfc.basket.items.IBasketItem;
import com.enactor.mfc.basket.items.ICardTenderItem;
import com.enactor.mfc.basket.items.ICashTenderItem;
import com.enactor.mfc.basket.items.ICustomerAccountTenderItem;
import com.enactor.mfc.basket.items.IDiscountItem;
import com.enactor.mfc.basket.items.IDispatchDetailsItem;
import com.enactor.mfc.basket.items.IGiftCardItem;
import com.enactor.mfc.basket.items.IGiftCardTenderItem;
import com.enactor.mfc.basket.items.IMerchandiseItem;
import com.enactor.mfc.basket.items.INonMerchandiseItem;
import com.enactor.mfc.basket.items.IPriceOverrideItem;
import com.enactor.mfc.basket.items.IQuantityModifierItem;
import com.enactor.mfc.basket.items.IServiceItem;
import com.enactor.mfc.basket.items.ITransactionDiscountItem;
import com.enactor.mfc.basket.items.ITransactionVoidItem;
import com.enactor.mfc.basket.items.IVoidItem;
import com.enactor.mfc.basket.items.IVoidableItem;
import com.enactor.pos.processes.PosActionFunctions;

public class CheckVoidItemType extends TestVoidItem {

	public CheckVoidItemType() {
	}

	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws ApplicationProcessException, UIProcessException {
		boolean voidAsChild = false;
		if (inputData.containsNotNullData(BasketDataTypes.VOID_AS_CHILD_DATA))
			voidAsChild = ((Boolean) inputData.getData(BasketDataTypes.VOID_AS_CHILD_DATA)).booleanValue();
		IBasket basket = PosActionFunctions.getModelBasketFromTransactionHandler(inputData);
		IBasketItem basketItem = PosActionFunctions.getBasketItem(inputData);
		if ((basketItem instanceof IVoidableItem) && ((IVoidableItem) basketItem).getVoided())
			return ALREADY_VOIDED;
		if ((basketItem instanceof IVoidItem) || (basketItem instanceof ITransactionVoidItem))
			return VOID_ITEM_OUTCOME;
		if (!testVoidable(basket, basketItem, voidAsChild))
			return CoreUIOutcomes.NOT_ALLOWED_OUTCOME;
		else
			return getVoidItemType(basketItem);
	}

	protected IApplicationProcessOutcome getVoidItemType(IBasketItem basketItem) {
		if (basketItem instanceof IMerchandiseItem)
			return MERCHANDISE_ITEM_VOID_OUTCOME;
		if (basketItem instanceof IAccountPaymentItem)
			return ACCOUNT_PAYMENT_ITEM_VOID_OUTCOME;
		if ((basketItem instanceof IGiftCardItem) || (basketItem instanceof IGiftCardTenderItem))
			return GIFT_CARD_ITEM_VOID_OUTCOME;
		if (basketItem instanceof IServiceItem)
			return SERVICE_ITEM_VOID_OUTCOME;
		if (basketItem instanceof INonMerchandiseItem)
			return NON_MERCHANDISE_ITEM_VOID_OUTCOME;
		if (basketItem instanceof ICardTenderItem)
			return CARD_TENDER_ITEM_VOID_OUTCOME;
		if (basketItem instanceof ICashTenderItem)
			return CASH_TENDER_ITEM_VOID_OUTCOME;
		if (basketItem instanceof ICustomerAccountTenderItem)
			return CUSTOMER_ACCOUNT_TENDER_ITEM_VOID_OUTCOME;
		if (basketItem instanceof IDiscountItem)
			return DISCOUNT_ITEM_VOID_OUTCOME;
		if (basketItem instanceof ITransactionDiscountItem)
			return TRANSACTION_DISCOUNT_VOID_OUTCOME;
		if (basketItem instanceof IPriceOverrideItem)
			return PRICE_OVERRIDE_VOID_OUTCOME;
		if (basketItem instanceof IQuantityModifierItem)
			return QUANTITY_MODIFIER_VOID_OUTCOME;
		if (basketItem instanceof IDispatchDetailsItem)
			return DISPATCH_DETAILS_ITEM_VOID_OUTCOME;
		if (basketItem instanceof IVoidableItem)
			return VOIDABLE_ITEM_VOID_OUTCOME;
		else
			return CoreUIOutcomes.FAIL_OUTCOME;
	}

	private static final long serialVersionUID = 1L;
	public static final String ALREADY_VOIDED_OUTCOME_NAME = "AlreadyVoided";
	public static final IApplicationProcessOutcome ALREADY_VOIDED = new ApplicationProcessOutcome("AlreadyVoided");
	public static final String VOID_ITEM_OUTCOME_NAME = "VoidItem";
	public static final IApplicationProcessOutcome VOID_ITEM_OUTCOME = new ApplicationProcessOutcome("VoidItem");
	public static final String VOIDABLE_ITEM_VOID_OUTCOME_NAME = "VoidableItemVoid";
	public static final IApplicationProcessOutcome VOIDABLE_ITEM_VOID_OUTCOME = new ApplicationProcessOutcome("VoidableItemVoid");
	public static final String MERCHANDISE_ITEM_VOID_OUTCOME_NAME = "MerchandiseItemVoid";
	public static final IApplicationProcessOutcome MERCHANDISE_ITEM_VOID_OUTCOME = new ApplicationProcessOutcome("MerchandiseItemVoid");
	public static final String NON_MERCHANDISE_ITEM_VOID_OUTCOME_NAME = "NonMerchandiseItemVoid";
	public static final IApplicationProcessOutcome NON_MERCHANDISE_ITEM_VOID_OUTCOME = new ApplicationProcessOutcome(
			"NonMerchandiseItemVoid");
	public static final String CARD_TENDER_ITEM_VOID_OUTCOME_NAME = "CardTenderItemVoid";
	public static final IApplicationProcessOutcome CARD_TENDER_ITEM_VOID_OUTCOME = new ApplicationProcessOutcome("CardTenderItemVoid");
	public static final String CASH_TENDER_ITEM_VOID_OUTCOME_NAME = "CashTenderItemVoid";
	public static final IApplicationProcessOutcome CASH_TENDER_ITEM_VOID_OUTCOME = new ApplicationProcessOutcome("CashTenderItemVoid");
	public static final String CUSTOMER_ACCOUNT_TENDER_ITEM_VOID_OUTCOME_NAME = "CustomerAccountTenderItemVoid";
	public static final IApplicationProcessOutcome CUSTOMER_ACCOUNT_TENDER_ITEM_VOID_OUTCOME = new ApplicationProcessOutcome(
			"CustomerAccountTenderItemVoid");
	public static final String DISCOUNT_ITEM_VOID_OUTCOME_NAME = "DiscountItemVoid";
	public static final IApplicationProcessOutcome DISCOUNT_ITEM_VOID_OUTCOME = new ApplicationProcessOutcome("DiscountItemVoid");
	public static final String TRANSACTION_DISCOUNT_VOID_OUTCOME_NAME = "TransactionDiscountVoid";
	public static final IApplicationProcessOutcome TRANSACTION_DISCOUNT_VOID_OUTCOME = new ApplicationProcessOutcome(
			"TransactionDiscountVoid");
	public static final String PRICE_OVERRIDE_VOID_OUTCOME_NAME = "PriceOverrideVoid";
	public static final IApplicationProcessOutcome PRICE_OVERRIDE_VOID_OUTCOME = new ApplicationProcessOutcome("PriceOverrideVoid");
	public static final String QUANTITY_MODIFIER_VOID_OUTCOME_NAME = "QuantityModifierVoid";
	public static final IApplicationProcessOutcome QUANTITY_MODIFIER_VOID_OUTCOME = new ApplicationProcessOutcome("QuantityModifierVoid");
	public static final String ACCOUNT_PAYMENT_ITEM_VOID_OUTCOME_NAME = "AccountPaymentVoid";
	public static final IApplicationProcessOutcome ACCOUNT_PAYMENT_ITEM_VOID_OUTCOME = new ApplicationProcessOutcome("AccountPaymentVoid");
	public static final String GIFT_CARD_ITEM_VOID_OUTCOME_NAME = "GiftCardItemVoid";
	public static final IApplicationProcessOutcome GIFT_CARD_ITEM_VOID_OUTCOME = new ApplicationProcessOutcome("GiftCardItemVoid");
	public static final String SERVICE_ITEM_VOID_OUTCOME_NAME = "ServiceItemVoid";
	public static final IApplicationProcessOutcome SERVICE_ITEM_VOID_OUTCOME = new ApplicationProcessOutcome("ServiceItemVoid");
	public static final String DISPATCH_DETAILS_ITEM_VOID_OUTCOME_NAME = "DispatchDetailsItemVoid";
	public static final IApplicationProcessOutcome DISPATCH_DETAILS_ITEM_VOID_OUTCOME = new ApplicationProcessOutcome(
			"DispatchDetailsItemVoid");
	public static final String ORDER_DETAILS_ITEM_VOID_OUTCOME_NAME = "OrderDetailsItemVoid";
	public static final IApplicationProcessOutcome ORDER_DETAILS_ITEM_VOID_OUTCOME = new ApplicationProcessOutcome("OrderDetailsItemVoid");

}
