package com.oneviewcommerce.actions.voids;

import java.util.Iterator;
import java.util.List;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.processes.CoreUIDataTypes;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.BasketDataTypes;
import com.enactor.mfc.basket.IBasket;
import com.enactor.mfc.basket.IBasketData;
import com.enactor.mfc.basket.IModifier;
import com.enactor.mfc.basket.items.DiscountModifier;
import com.enactor.mfc.basket.items.IAlterationModifier;
import com.enactor.mfc.basket.items.IBasketItem;
import com.enactor.mfc.basket.items.IBasketItemModifier;
import com.enactor.mfc.basket.items.ICollectModifier;
import com.enactor.mfc.basket.items.IDiscountItem;
import com.enactor.mfc.basket.items.IDispatchModifier;
import com.enactor.mfc.basket.items.IEmployeeDiscountModifier;
import com.enactor.mfc.basket.items.IEmployeeSaleTransactionDiscountItem;
import com.enactor.mfc.basket.items.IModifiableItem;
import com.enactor.mfc.basket.items.IModifierItem;
import com.enactor.mfc.basket.items.IPriceOverrideItem;
import com.enactor.mfc.basket.items.IPriceOverrideModifier;
import com.enactor.mfc.basket.items.IPromotionModifier;
import com.enactor.mfc.basket.items.ISingleModifierItem;
import com.enactor.mfc.basket.items.ITransactionDiscountItem;
import com.enactor.mfc.basket.items.IValueModifier;
import com.enactor.mfc.basket.items.IVoidableItem;
import com.enactor.mfc.basket.items.ModifierType;
import com.enactor.mfc.retail.transaction.IRetailTransactionHandler;

public class TestVoidItem implements IUIAction {

	public TestVoidItem() {
	}

	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws ApplicationProcessException, UIProcessException {
		IBasket basket = null;
		if (inputData.containsNotNullData(BasketDataTypes.BASKET_DATA))
			basket = (IBasket) inputData.getData(BasketDataTypes.BASKET_DATA);
		else if (inputData.containsNotNullData(BasketDataTypes.TRANSACTION_HANDLER_DATA)) {
			IRetailTransactionHandler transactionHandler = (IRetailTransactionHandler) inputData
					.getData(BasketDataTypes.TRANSACTION_HANDLER_DATA);
			if (transactionHandler.getModel() instanceof IBasketData)
				basket = ((IBasketData) transactionHandler.getModel()).getBasket();
		}
		IBasketItem basketItem = null;
		if (inputData.containsNotNullData(BasketDataTypes.BASKET_ITEM_DATA))
			basketItem = (IBasketItem) inputData.getData(BasketDataTypes.BASKET_ITEM_DATA);
		else if (inputData.containsNotNullData(CoreUIDataTypes.SELECTED_ITEM_DATA)) {
			Object selectedItem = inputData.getData(CoreUIDataTypes.SELECTED_ITEM_DATA);
			if (selectedItem instanceof IBasketItem)
				basketItem = (IBasketItem) selectedItem;
		}
		boolean voidAsChild = false;
		if (inputData.containsNotNullData(BasketDataTypes.VOID_AS_CHILD_DATA))
			voidAsChild = ((Boolean) inputData.getData(BasketDataTypes.VOID_AS_CHILD_DATA)).booleanValue();
		if (testVoidable(basket, basketItem, voidAsChild))
			return CoreUIOutcomes.SUCCESS_OUTCOME;
		else
			return CoreUIOutcomes.FAIL_OUTCOME;
	}

	protected boolean testVoidable(IBasket basket, IBasketItem basketItem) {
		return testVoidable(basket, basketItem, false);
	}

	protected boolean testVoidable(IBasket basket, IBasketItem basketItem, boolean voidAsChild) {
		label0: {
			if (basket == null || basket.isEmpty())
				return false;
			if (basketItem == null)
				return false;
			if (!basket.containsItem(basketItem))
				return false;
			if (!(basketItem instanceof IVoidableItem))
				return false;
			IVoidableItem voidableItem = (IVoidableItem) basketItem;
			if (voidableItem.getNotVoidable())
				return false;
			if (voidableItem.getVoided())
				return false;
			if (!voidAsChild && basketItem.getParent() != null && !voidableItem.getAllowVoidWhenChild())
				return false;
			if (basketItem instanceof IDiscountItem) {
				IDiscountItem discountItem = (IDiscountItem) basketItem;
				IValueModifier valueModifier = (IValueModifier) discountItem.getModifier();
				IModifiableItem modifiableItem = discountItem.getModifiedItem();
				int discountModifierIndex = modifiableItem.getModifierIndex(valueModifier);
				if (discountModifierIndex >= 0) {
					List<IModifier> modifiers = modifiableItem.getModifiers();
					for (int i = discountModifierIndex + 1; i < modifiers.size(); i++)
						if (modifiers.get(i) instanceof IBasketItemModifier) {
							IModifierItem modifierItem = ((IBasketItemModifier) modifiers.get(i)).getModifierItem();
							if (modifierItem != null && !modifierItem.getVoided() && modifierItem.getType() == ModifierType.PERCENTAGE)
								return false;
						}

				} else {
					try {
						throw new Exception();
					} catch (Exception e) {
						logger.log(1000, "Discount Modifier not found in list of modifiers.", e);
					}
					return false;
				}
			} else {
				if (basketItem instanceof IEmployeeSaleTransactionDiscountItem)
					return false;
				if (basketItem instanceof ITransactionDiscountItem) {
					ITransactionDiscountItem transDiscountItem = (ITransactionDiscountItem) basketItem;
					List<IModifiableItem> modifiedItems = transDiscountItem.getModifiedItems();
					for (Iterator<IModifiableItem> i$ = modifiedItems.iterator(); i$.hasNext();) {
						IModifiableItem modifiableItem = i$.next();
						Iterator<IValueModifier> j$ = transDiscountItem.getModifiers(modifiableItem).iterator();
						while (j$.hasNext()) {
							IModifier modifier = (IValueModifier) j$.next();
							int discountModifierIndex = modifiableItem.getModifierIndex(modifier);
							if (discountModifierIndex >= 0) {
								List<IModifier> modifiers = modifiableItem.getModifiers();
								int i = discountModifierIndex + 1;
								while (i < modifiers.size()) {
									if (modifiers.get(i) instanceof IBasketItemModifier) {
										IModifierItem modifierItem = ((IBasketItemModifier) modifiers.get(i)).getModifierItem();
										if (modifierItem != null && !modifierItem.getVoided()
												&& modifierItem.getType() == ModifierType.PERCENTAGE)
											return false;
									}
									i++;
								}
							} else {
								try {
									throw new Exception();
								} catch (Exception e) {
									logger.log(1000, "Discount Modifier not found in list of modifiers.", e);
								}
								return false;
							}
						}
					}

				}
			}
			IModifier modifier;
			if (basketItem instanceof IPriceOverrideItem) {
				IPriceOverrideItem overrideItem = (IPriceOverrideItem) basketItem;
				modifier = (IPriceOverrideModifier) overrideItem.getModifier();
				IModifiableItem modifiableItem = overrideItem.getModifiedItem();
				int modifierIndex = modifiableItem.getModifierIndex(modifier);
				if (modifierIndex >= 0) {
					List<IModifier> modifiers = modifiableItem.getModifiers();
					for (int i = modifierIndex + 1; i < modifiers.size(); i++)
						if (modifiers.get(i) instanceof IBasketItemModifier) {
							IModifierItem modifierItem = ((IBasketItemModifier) modifiers.get(i)).getModifierItem();
							if (modifierItem != null && !modifierItem.getVoided() && modifierItem.getType() == ModifierType.PERCENTAGE)
								return false;
						}

				} else {
					try {
						throw new Exception();
					} catch (Exception e) {
						logger.log(1000, "Modifier not found in list of modifiers.", e);
					}
					return false;
				}
				break label0;
			}
			if (!(basketItem instanceof IModifiableItem))
				break label0;
			Iterator<IModifier> i$ = ((IModifiableItem) basketItem).getModifiers().iterator();
			do {
				if (!i$.hasNext())
					break label0;
				modifier = (IModifier) i$.next();
			} while (modifier.getVoided() || !(modifier instanceof IBasketItemModifier) || (modifier instanceof IPromotionModifier)
					|| (modifier instanceof ICollectModifier) || (modifier instanceof IDispatchModifier)
					|| (modifier instanceof IEmployeeDiscountModifier) || ((IBasketItemModifier) modifier).getModifierItem() == null
					|| (((IBasketItemModifier) modifier).getModifierItem() instanceof ISingleModifierItem)
					|| (modifier instanceof IAlterationModifier) || (modifier instanceof DiscountModifier));
			return false;
		}
		return true;
	}

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(TestVoidItem.class.getName());

}
