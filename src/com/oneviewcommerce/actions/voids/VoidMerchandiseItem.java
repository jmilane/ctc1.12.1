package com.oneviewcommerce.actions.voids;

import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessDataType;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.signOn.ISignedOnUser;
import com.enactor.coreUI.actions.IUIActionMetaData;
import com.enactor.coreUI.actions.UIActionFunctions;
import com.enactor.coreUI.actions.UIActionMetaData;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.BasketDataTypes;
import com.enactor.mfc.basket.IBasket;
import com.enactor.mfc.basket.items.IMerchandiseItem;
import com.enactor.mfc.basket.items.IVoidItem;
import com.enactor.mfc.basket.items.VoidItem;
import com.enactor.mfc.device.DeviceDataTypes;
import com.enactor.mfc.device.IDevice;
import com.enactor.pos.packages.basket.processes.BasketItemHelper;
import com.enactor.pos.processes.PosActionFunctions;

// Referenced classes of package com.enactor.pos.packages.voids.processes:
//            TestVoidItem

public class VoidMerchandiseItem extends TestVoidItem {

	public VoidMerchandiseItem() {
	}

	public IUIActionMetaData getActionMetaData() {
		return new UIActionMetaData(getActionOutcomes(), getInputTypes(), getOutputTypes());
	}

	protected IApplicationProcessOutcome[] getActionOutcomes() {
		return (new IApplicationProcessOutcome[] { CoreUIOutcomes.SUCCESS_OUTCOME, CoreUIOutcomes.FAIL_OUTCOME,
				CoreUIOutcomes.NOT_ALLOWED_OUTCOME });
	}

	protected IApplicationProcessDataType[] getInputTypes() {
		return (new IApplicationProcessDataType[] { BasketDataTypes.TRANSACTION_HANDLER_DATA, BasketDataTypes.MERCHANDISE_ITEM_DATA,
				BasketDataTypes.VOID_AS_CHILD_DATA });
	}

	protected IApplicationProcessDataType[] getOutputTypes() {
		return (new IApplicationProcessDataType[] { BasketDataTypes.VOID_ITEM_DATA });
	}

	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws ApplicationProcessException, UIProcessException {
		IBasket basket = PosActionFunctions.getModelBasketFromTransactionHandler(inputData);
		IMerchandiseItem merchandiseItem = PosActionFunctions.getMerchandiseItem(inputData);
		ISignedOnUser user = PosActionFunctions.getSignedOnUser(inputData);
		IDevice device = (IDevice) UIActionFunctions.getRequiredDataItem(inputData, DeviceDataTypes.DEVICE_DATA);
		boolean voidAsChild = false;
		if (inputData.containsNotNullData(BasketDataTypes.VOID_AS_CHILD_DATA))
			voidAsChild = ((Boolean) inputData.getData(BasketDataTypes.VOID_AS_CHILD_DATA)).booleanValue();
		if (!testVoidable(basket, merchandiseItem, voidAsChild)) {
			return CoreUIOutcomes.NOT_ALLOWED_OUTCOME;
		} else {
			IVoidItem voidItem = (IVoidItem) BasketItemHelper.makeBasketItem(VoidItem.ENTITY_QNAME, device.getDeviceId(), user.getUserId());
			voidItem.setModifiedItem(merchandiseItem);
			voidItem.setVoidQuantity(merchandiseItem.getQuantity());
			voidItem.setVoidedLine(merchandiseItem.getLineNumber());
			voidItem.setVoidedItem(merchandiseItem);
			outputData.setData(BasketDataTypes.VOID_ITEM_DATA, voidItem);
			return CoreUIOutcomes.SUCCESS_OUTCOME;
		}
	}

	private static final long serialVersionUID = 1L;
}
