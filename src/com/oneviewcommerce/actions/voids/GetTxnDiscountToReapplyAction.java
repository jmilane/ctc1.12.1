package com.oneviewcommerce.actions.voids;

import com.enactor.core.application.process.ApplicationProcessDataType;
import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessDataType;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.objectCache.CacheManager;
import com.enactor.core.transaction.TransactionException;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessContext;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.basket.items.TransactionDiscountItem;
import com.enactor.mfc.retail.transaction.NormalRetailSaleTransactionHandler;
import com.enactor.pos.processes.PosActionFunctions;
import com.oneviewcommerce.actions.basket.CTCTransactionDiscountItemProcessor;

public class GetTxnDiscountToReapplyAction implements IUIAction {

	private static final long serialVersionUID = 1L;

	public IApplicationProcessOutcome execute(IApplicationProcessData inputData, IApplicationProcessData outputData)
			throws UIProcessException, ApplicationProcessException {

		String cacheId = null;
		if (UIProcessContext.getContext() != null) {
			cacheId = (new StringBuilder()).append(UIProcessContext.getContext().getProcessRunner().getView().getInstanceId()).append(":")
					.append("EntityCache").toString();

			TransactionDiscountItem txnDiscountItem = CacheManager.getCacheEntity(cacheId, "txnDiscountItem");

			if (txnDiscountItem != null) {

				IApplicationProcessDataType transactionDiscountItem = new ApplicationProcessDataType("fgl.pos.TxnDiscountToReapply",
						com.enactor.mfc.basket.items.TransactionDiscountItem.class.getName());

				if (transactionDiscountItem != null) {
					NormalRetailSaleTransactionHandler transactionHandler = (NormalRetailSaleTransactionHandler) PosActionFunctions
							.getTransactionHandler(inputData);
					try {
						transactionHandler.processTransactionItem(txnDiscountItem, new CTCTransactionDiscountItemProcessor());
					} catch (TransactionException ex) {
						throw new UIProcessException(ex);
					}
				}
				outputData.setData(transactionDiscountItem, txnDiscountItem);
			}
		}
		return CoreUIOutcomes.SUCCESS_OUTCOME;
	}

}
