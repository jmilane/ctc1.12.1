package com.oneviewcommerce.actions.search;

import com.enactor.core.application.process.ApplicationProcessDataType;
import com.enactor.core.application.process.ApplicationProcessException;
import com.enactor.core.application.process.IApplicationProcessData;
import com.enactor.core.application.process.IApplicationProcessDataType;
import com.enactor.core.application.process.IApplicationProcessOutcome;
import com.enactor.core.entities.PropertyValidationException;
import com.enactor.core.utilities.Logger;
import com.enactor.coreUI.actions.IUIAction;
import com.enactor.coreUI.actions.UIActionFunctions;
import com.enactor.coreUI.processes.CoreUIOutcomes;
import com.enactor.coreUI.processes.UIProcessException;
import com.enactor.mfc.product.productSearch.IProductSearchCriteria;

public class UppercaseProductIdAction implements IUIAction {

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(UppercaseProductIdAction.class.getName());

	public IApplicationProcessOutcome execute(
			IApplicationProcessData inputData,
			IApplicationProcessData outputData) throws UIProcessException,
			ApplicationProcessException {

		IProductSearchCriteria productSearchCriteria = (IProductSearchCriteria) UIActionFunctions.getRequiredDataItem(inputData,
		"enactor.pos.ProductSearchCriteria");
		try{
			// This is the whole point.
			// We're setting the product id to uppercase so that the search is not case sensitive  
			productSearchCriteria.setProductCode(productSearchCriteria.getProductCode().toUpperCase());
		}
		catch(PropertyValidationException ex){
			logger.log(Logger.LOG_ERROR, "Error in com.oneviewcommerce.actions.search.UppercaseProductId: " + ex.getMessage() + ".");
			throw new UIProcessException(ex);
		}

		IApplicationProcessDataType productSearchCriteriaOutput = new ApplicationProcessDataType("enactor.pos.ProductSearchCriteria",
				IProductSearchCriteria.class.getName());
		outputData.setData(productSearchCriteriaOutput, productSearchCriteria);
		return CoreUIOutcomes.SUCCESS_OUTCOME;

	}

}
